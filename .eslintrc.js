module.exports = {
  env: {
    es2021: true
  },
  'extends': [
    'plugin:vue/essential',
    'eslint:recommended'
  ],
  rules: {
    'no-unused-vars': 'warn',
    'vue/multi-word-component-names': 0,
    'vue/no-mutating-props': 0
  }
}
