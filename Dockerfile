FROM node:16.16.0-alpine3.15 as build-stage
WORKDIR /app

COPY ./package*.json ./

RUN npm install

COPY . .

RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage

COPY --from=build-stage /app/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build-stage /app/sastro_id_ssl.crt /etc/nginx/sastro_id_ssl.crt
COPY --from=build-stage /app/sastro_id.key /etc/nginx/sastro_id.key
COPY --from=build-stage /app/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
