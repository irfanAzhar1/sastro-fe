import { defineConfig } from "vite";
import { createVuePlugin as vue } from "vite-plugin-vue2";
import { VuetifyResolver } from "unplugin-vue-components/resolvers";
import Components from "unplugin-vue-components/vite";
import eslintPlugin from "vite-plugin-eslint";

// eslint-disable-next-line no-undef
const path = require("path");
export default defineConfig({
  plugins: [
    vue(),
    Components({ resolvers: [VuetifyResolver()] }),
    eslintPlugin(),
  ],

  resolve: {
    alias: [
      {
        find: "eventsource",
        replacement:
          "./node_modules/sockjs-client/lib/transport/browser/eventsource.js",
      },
      {
        find: "events",
        replacement: "./node_modules/sockjs-client/lib/event/emitter.js",
      },
      {
        find: "crypto",
        replacement: "./node_modules/sockjs-client/lib/utils/browser-crypto.js",
      },
      {
        find: "@",
        replacement: path.resolve(__dirname, "./src"),
      },
    ],
    extensions: [".mjs", ".js", ".ts", ".jsx", ".tsx", ".json", ".vue"],
    define: {
      global: {},
    },
  },
});
