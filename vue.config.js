module.exports = {
  transpileDependencies: [
    'vuetify', '@peepi/vuetify-tiptap'
  ],
  devServer: {
    disableHostCheck: true,
    open: process.platform === 'darwin',
    host: '0.0.0.0',
    port: 3001, // CHANGE YOUR PORT HERE!
    https: false,
    hotOnly: false,
  },
  publicPath:"/"
}
