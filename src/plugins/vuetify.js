import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#4267B2",
        background: "#EEE",
        secondary: "#f3f3f9",
      },
    },
  },
});
