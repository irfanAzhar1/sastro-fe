import axios from "axios";
import store from "../store/index";
import router from "../router";

axios.interceptors.request.use(
  function (config) {
    let user = JSON.parse(localStorage.getItem("token"));
    if (user && user.accessToken) {
      config.headers["Authorization"] = "Bearer " + user.accessToken;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (err) {
    const originalConfig = err.config;
    if (err.response.status === 401 && !originalConfig._retry) {
      originalConfig._retry = true;
      const token = JSON.parse(localStorage.getItem("token"));
      try {
        axios
          .post(import.meta.env.VITE_AUTH_SERVICE_URL + "refreshToken", {
            refreshToken: token.refreshToken,
          })
          .then((e) => {
            const { accessToken, refreshToken } = e.data.data;
            let newAccessToken = {
              ...token,
              accessToken: accessToken,
              refreshToken: refreshToken,
            };
            localStorage.setItem("token", JSON.stringify(newAccessToken));
          })
          .catch(() => {
            router.replace("/login");
            store.dispatch("auth/logout");
          });
        return axios(originalConfig);
      } catch (_error) {
        return Promise.reject(_error);
      }
    }
    return Promise.reject(err);
  }
);

export default axios;
