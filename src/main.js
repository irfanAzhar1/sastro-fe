import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import Loading from "vue-loading-overlay";
import VeeValidate from "vee-validate";
import VNumeric from "vuetify-numeric/vuetify-numeric.umd";
import VueSweetalert2 from "vue-sweetalert2";

import "sweetalert2/dist/sweetalert2.min.css";
import VTiptap from "@peepi/vuetify-tiptap";
import VueClipboard from "vue-clipboard2";

Vue.use(Loading);

Vue.use(VeeValidate);

Vue.use(VNumeric);

Vue.use(VueSweetalert2);

Vue.use(VTiptap);

VueClipboard.config.autoSetContainer = true;
Vue.use(VueClipboard);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
