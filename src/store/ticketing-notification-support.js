import FSupportTicketService from "../modules/ticketing/services/f-support-ticket-service";

export const ticketing = {
  namespaced: true,
  state: {
    itemTicketingNotification: [],
    itemTotalTicketingNotification: 0,
  },
  actions: {
    loadTicketingNotification({ commit }) {
      this.itemTotalUnreadTicketingNotification = 0;
      FSupportTicketService.getSupportTicketConversationPaginated(
        1,
        5,
        "createdAt",
        "DESC"
      ).then(
        (response) => {
          commit("loadTicketingNotification", response.data.content);
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    },
  },
  mutations: {
    loadTicketingNotification(state, items) {
      state.itemTicketingNotification = items;
    },
  },
};
