import FDivisionService from "../modules/division/services/f-division-service";
import FCustomerService from "../modules/mitra/services/customer_service/f-customer-service";
import FMaterialService from "../modules/material/services/f-material-service";
import FExpedisiService from "../modules/mitra/services/ekspedisi_service/f-expedisi-service";
import FWarehouseService from "../modules/inventory/services/f-warehouse-service";
import FSalesmanService from "../modules/pegawai/services/f-salesman-service";
import FPayMethodService from "../modules/paymethod/services/f-pay-method-service";
import FStoreService from "../modules/mitra/services/f-store-service";

import FSubAreaService from "../modules/mitra/services/area_service/f-sub-area-service";
import FAreaService from "../modules/mitra/services/area_service/f-area-service";
import FRegionService from "../modules/mitra/services/area_service/f-region-service";
import FDistributionChannelService from "../modules/mitra/services/f-distribution-channel-service";
import FCustomerGroupService from "../modules/mitra/services/customer_service/f-customer-group-service";
import FMaterialGroup2Service from "../modules/material/services/f-material-group2-service";
import FMaterialGroup3Service from "../modules/material/services/f-material-group3-service";
import FMaterialGroup1Service from "../modules/material/services/f-material-group1-service";
import FMaterialSalesBrandService from "../modules/material/services/f-material-sales-brand-service";
import FVendorService from "../modules/mitra/services/f-vendor-service";
import FWarehouseExpedisiService from "../modules/inventory/services/f-warehouse-expedisi-service";
import FCategoryTicketService from "../modules/ticketing/services/f-category-ticket-service";
import FCategoryTicketEkspedisiService from "../modules/ticketing/services/f-category-ticket-expedisi-service";
import ERole from "@/models/e-role";
import FDiscountService from "@/modules/discount/services/f-discount-service";

export const data = {
  namespaced: true,
  state: {
    bg: "#FFF",
    tempUserOrder: {
      salesman_id: null,
      store_id: null,
    },
    listFDivision: [],
    listFCustomer: [],

    listFExpedisi: [],
    listFWarehouse: [],

    /* all salesman */
    listFSalesman: [],
    /* salesman user*/
    listFSalesmanUser: [],

    /* all method */
    listFPayMethod: [],
    /* all method by ekspedisi kode1 */
    listFPayMethodByEkspedisi: [],

    listFStore: [],
    listDiscount: [],
    listFSubArea: [],
    listFArea: [],
    listFRegion: [],

    listFDistributionChannel: [],
    listFCustomerGroup: [],

    listFMaterial: [],
    listFMaterialGroup3: [],
    listFMaterialGroup2: [],
    listFMaterialGroup1: [],
    listFMaterialSalesBrand: [],
    listFVendor: [],

    listFWarehouseExpedisiAvailable: [],
    listFTicketCategory: [],
    listFTicketCategoryEkspedisi: [],
  },

  actions: {
    tempOrder({ commit }, temp) {
      commit("tempOrder", temp);
    },
    getSalesmanByRole({ commit }, currentUser) {
      commit("getSalesmanByRole", currentUser);
    },

    loadMetodePembayaranByEkpedisi({ commit }, ekspedisiKode1) {
      commit("loadMetodePembayaranByEkpedisi", ekspedisiKode1);
    },

    loadBackground({ commit }, bg) {
      commit("loadBackground", bg);
    },

    loadFTicketCategoryEkspedisi({ commit }) {
      FCategoryTicketEkspedisiService.getAllCategoryExpedisi().then(
        (response) => {
          commit("loadFTicketCategoryEkspedisiSuccess", response.data.data);
        },
        (error) => {
          commit("loadFTicketCategoryEkspedisiFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFTicketCategory({ commit }) {
      FCategoryTicketService.getAllCategory().then(
        (response) => {
          commit("loadFTicketCategorySuccess", response.data);
        },
        (error) => {
          commit("loadFTicketCategoryFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFDivisionByOrgLevel({ commit }) {
      FDivisionService.getAllFDivisionByOrgLevel().then(
        (response) => {
          commit("loadFDivisionByOrgLevelSuccess", response.data);
        },
        (error) => {
          commit("loadFDivisionByOrgLevelFailure");
          return Promise.reject(error);
        }
      );
    },

    loadFDivision({ commit }) {
      FDivisionService.getAllFDivisionByOrgLevel().then(
        (response) => {
          commit("loadFDivisionSuccess", response.data);
        },
        (error) => {
          commit("loadFDivisionFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFCustomer({ commit }, payload) {
      let fsalesmanBean = payload.fsalesmanBean;

      if (fsalesmanBean > 0) {
        // FCustomerService.getAllFCustomerBySalesman(fsalesmanBean).then(
        //     response =>{
        //         commit('loadFCustomerSuccess', response.data)
        //     },
        //     error =>{
        //         commit('loadFCustomerFailure')
        //         return Promise.reject(error)
        //     }
        // )

        FCustomerService.getAllFCustomerBySalesmanAndCustnameContaining(
          fsalesmanBean,
          ""
        ).then(
          (response) => {
            commit("loadFCustomerSuccess", response.data);
          },
          (error) => {
            commit("loadFCustomerFailure");
            return Promise.reject(error);
          }
        );
      } else {
        FCustomerService.getAllFCustomerSimple().then(
          (response) => {
            commit("loadFCustomerSuccess", response.data);
          },
          (error) => {
            commit("loadFCustomerFailure");
            return Promise.reject(error);
          }
        );
      }
    },
    loadFExpedisi({ commit }) {
      FExpedisiService.getAllFExpedisiByCompany().then(
        (response) => {
          commit("loadFExpedisiSuccess", response.data);
        },
        (error) => {
          commit("loadFExpedisiFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFWarehouse({ commit }) {
      FWarehouseService.getAllFWarehouseByCompany().then(
        (response) => {
          commit("loadFWarehouseSuccess", response.data);
        },
        (error) => {
          commit("loadFWarehouseFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFSalesman({ commit }) {
      FSalesmanService.getAllFSalesmanSimple().then(
        (response) => {
          commit("loadFSalesmanSuccess", response.data);
        },
        (error) => {
          commit("loadFSalesmanFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFSalesmanChildrenAndMe({ commit }) {
      FSalesmanService.getAllMyAgenChildrenAndMe().then(
        (response) => {
          commit("loadFSalesmanSuccess", response.data);
        },
        (error) => {
          commit("loadFSalesmanFailure");
          return Promise.reject(error);
        }
      );
    },

    loadFPayMethod({ commit }) {
      FPayMethodService.getAllFPayMethodByCompany().then(
        (response) => {
          commit("loadFPayMethodSuccess", response.data);
        },
        (error) => {
          commit("loadFPayMethodFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFStore({ commit }) {
      FStoreService.getAllFStoreSimple().then(
        (response) => {
          commit("loadFStoreSuccess", response.data);
        },
        (error) => {
          commit("loadFStoreFailure");
          return Promise.reject(error);
        }
      );
    },
    loadDiscount({ commit }) {
      FDiscountService.getAllActive().then(
        (response) => {
          commit("loadDiscountSuccess", response.data.data);
        },
        (error) => {
          commit("loadDiscountFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFSubArea({ commit }) {
      FSubAreaService.getAllFSubAreaSimple().then(
        (response) => {
          commit("loadFSubAreaSuccess", response.data);
        },
        (error) => {
          commit("loadFSubAreaFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFArea({ commit }) {
      FAreaService.getAllFAreaSimple().then(
        (response) => {
          commit("loadFAreaSuccess", response.data);
        },
        (error) => {
          commit("loadFAreaFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFRegion({ commit }) {
      FRegionService.getAllFRegion().then(
        (response) => {
          commit("loadFRegionSuccess", response.data);
        },
        (error) => {
          commit("loadFRegionFailure");
          return Promise.reject(error);
        }
      );
    },

    loadFDistributionChannel({ commit }) {
      FDistributionChannelService.getAllFDistributionChannelByCompanySimple().then(
        (response) => {
          commit("loadFDistributionChannelSuccess", response.data);
        },
        (error) => {
          commit("loadFDistributionChannelFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFCustomerGroup({ commit }) {
      FCustomerGroupService.getAllFCustomerGroupByCompanySimple().then(
        (response) => {
          commit("loadFCustomerGroupSuccess", response.data);
        },
        (error) => {
          commit("loadFCustomerGroupFailure");
          return Promise.reject(error);
        }
      );
    },

    loadFMaterial({ commit }) {
      FMaterialService.getAllFMaterialFilter().then(
        (response) => {
          commit("loadFMaterialSuccess", response.data.data);
        },
        (error) => {
          commit("loadFMaterialFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFMaterialGroup3({ commit }) {
      FMaterialGroup3Service.getAllFMaterialGroup3().then(
        (response) => {
          commit("loadFMaterialGroup3Success", response.data);
        },
        (error) => {
          commit("loadFMaterialGroup3Failure");
          return Promise.reject(error);
        }
      );
    },
    loadFMaterialGroup2({ commit }) {
      FMaterialGroup2Service.getAllFMaterialGroup2().then(
        (response) => {
          commit("loadFMaterialGroup2Success", response.data);
        },
        (error) => {
          commit("loadFMaterialGroup2Failure");
          return Promise.reject(error);
        }
      );
    },
    loadFMaterialGroup1({ commit }) {
      FMaterialGroup1Service.getAllFMaterialGroup1().then(
        (response) => {
          commit("loadFMaterialGroup1Success", response.data);
        },
        (error) => {
          commit("loadFMaterialGroup1Failure");
          return Promise.reject(error);
        }
      );
    },
    loadFMaterialSalesBrand({ commit }) {
      FMaterialSalesBrandService.getAllFMaterialSalesBrand().then(
        (response) => {
          commit("loadFMaterialSalesBrandSuccess", response.data);
        },
        (error) => {
          commit("loadFMaterialSalesBrandFailure");
          return Promise.reject(error);
        }
      );
    },
    loadFVendor({ commit }) {
      FVendorService.getAllFVendor().then(
        (response) => {
          commit("loadFVendorSuccess", response.data);
        },
        (error) => {
          commit("loadFVendorFailure");
          return Promise.reject(error);
        }
      );
    },

    loadFWarehouseExpedisiAvailable({ commit }) {
      FWarehouseExpedisiService.getAllFWarehouseExpedisiAvailable().then(
        (response) => {
          // console.log(JSON.stringify(response.data))

          commit("loadFWarehouseExpedisiAvailableSuccess", response.data);
        },
        (error) => {
          commit("loadFWarehouseExpedisiAvailableFailure");
          return Promise.reject(error);
        }
      );
    },
  },
  mutations: {
    tempOrder(state, temp) {
      state.tempUserOrder.salesman_id = temp.salesman_id;
      state.tempUserOrder.store_id = temp.store_id;
    },
    getSalesmanByRole(state, currentUser) {
      let listFSalesman = [];

      if (
        currentUser.salesmanOf > 0 &&
        [
          ERole.ROLE_CS,
          ERole.ROLE_AGEN,
          ERole.ROLE_RSL_1,
          ERole.ROLE_RSL_2,
        ].some((x) => currentUser.roles.includes(x))
      ) {
        state.listFSalesman
          .filter((x) => x.statusActive)
          .forEach((item) => {
            listFSalesman.push(item);
          });
      } else {
        state.listFSalesman
          .filter((x) => x.statusActive)
          .forEach((item) => {
            listFSalesman.push(item);
          });
      }

      /* khusus role piutang, hanya bisa pakai dirinya */
      if (
        currentUser.roles.includes(ERole.ROLE_PIUTANG) &&
        currentUser.salesmanOf > 0
      ) {
        listFSalesman = state.listFSalesman.filter(
          (x) => x.statusActive && x.id === currentUser.salesmanOf
        );
      }

      state.listFSalesmanUser = listFSalesman;
    },

    loadMetodePembayaranByEkpedisi(state, ekpedisiKode1) {
      let query = "";

      if (ekpedisiKode1.includes("COD")) {
        query = "COD";
      } else if (ekpedisiKode1.includes("cash")) {
        query = "WAREHOUSE";
      } else {
        query = "REG";
      }

      let data = state.listFPayMethod.filter((row) => row.statusActive);
      let temp = data;

      if (query === "COD") {
        temp = data.filter((row) => row.kode1 === "COD");
      } else if (query === "REG") {
        temp = data.filter(
          (row) => row.kode1 !== "COD" && row.kode1 !== "CASH"
        );
      } else {
        temp = data.filter((row) => row.kode1 === "CASH");
      }

      state.listFPayMethodByEkspedisi = temp;
    },

    loadBackground(state, items) {
      state.bg = items;
    },
    loadFTicketCategoryEkspedisiSuccess(state, items) {
      state.listFTicketCategoryEkspedisi = items;
    },
    loadFTicketCategoryEkspedisiFailure(state) {
      state.listFTicketCategoryEkspedisi = [];
    },
    loadFTicketCategorySuccess(state, items) {
      state.listFTicketCategory = items;
    },
    loadFTicketCategoryFailure(state) {
      state.listFTicketCategory = [];
    },
    loadFDivisionByOrgLevelSuccess(state, items) {
      state.listFDivision = items;
    },
    loadFDivisionByOrgLevelFailure(state) {
      state.listFDivision = [];
    },

    loadFDivisionSuccess(state, items) {
      state.listFDivision = items;
    },
    loadFDivisionFailure(state) {
      state.listFDivision = [];
    },
    loadFCustomerSuccess(state, items) {
      state.listFCustomer = items;
    },
    loadFCustomerFailure(state) {
      state.listFCustomer = [];
    },
    loadFExpedisiSuccess(state, items) {
      state.listFExpedisi = items;
    },
    loadFExpedisiFailure(state) {
      state.listFExpedisi = [];
    },
    loadFWarehouseSuccess(state, items) {
      state.listFWarehouse = items;
    },
    loadFWarehouseFailure(state) {
      state.listFWarehouse = [];
    },
    loadFSalesmanSuccess(state, items) {
      state.listFSalesman = items;
    },
    loadFSalesmanFailure(state) {
      state.listFSalesman = [];
    },
    loadFPayMethodSuccess(state, items) {
      state.listFPayMethod = items;
    },
    loadFPayMethodFailure(state) {
      state.listFPayMethod = [];
    },
    loadFStoreSuccess(state, items) {
      state.listFStore = items;
    },
    loadDiscountSuccess(state, items) {
      state.listDiscount = items;
    },
    loadDiscountFailure(state) {
      state.listDiscount = [];
    },
    loadFStoreFailure(state) {
      state.listFStore = [];
    },

    loadFSubAreaSuccess(state, items) {
      state.listFSubArea = items;
    },
    loadFSubAreaFailure(state) {
      state.listFSubArea = [];
    },

    loadFAreaSuccess(state, items) {
      state.listFArea = items;
    },
    loadFAreaFailure(state) {
      state.listFArea = [];
    },
    loadFRegionSuccess(state, items) {
      state.listFRegion = items;
    },
    loadFRegionFailure(state) {
      state.listFRegion = [];
    },

    loadFDistributionChannelSuccess(state, items) {
      state.listFDistributionChannel = items;
    },
    loadFDistributionChannelFailure(state) {
      state.listFDistributionChannel = [];
    },
    loadFCustomerGroupSuccess(state, items) {
      state.listFCustomerGroup = items;
    },
    loadFCustomerGroupFailure(state) {
      state.listFCustomerGroup = [];
    },

    loadFMaterialSuccess(state, items) {
      state.listFMaterial = items;
    },
    loadFMaterialFailure(state) {
      state.listFMaterial = [];
    },
    loadFMaterialGroup3Success(state, items) {
      state.listFMaterialGroup3 = items;
    },
    loadFMaterialGroup3Failure(state) {
      state.listFMaterialGroup3 = [];
    },
    loadFMaterialGroup2Success(state, items) {
      state.listFMaterialGroup2 = items;
    },
    loadFMaterialGroup2Failure(state) {
      state.listFMaterialGroup2 = [];
    },
    loadFMaterialGroup1Success(state, items) {
      state.listFMaterialGroup1 = items;
    },
    loadFMaterialGroup1Failure(state) {
      state.listFMaterialGroup1 = [];
    },
    loadFMaterialSalesBrandSuccess(state, items) {
      state.listFMaterialSalesBrand = items;
    },
    loadFMaterialSalesBrandFailure(state) {
      state.listFMaterialSalesBrand = [];
    },
    loadFVendorSuccess(state, items) {
      state.listFVendor = items;
    },
    loadFVendorFailure(state) {
      state.listFVendor = [];
    },

    loadFWarehouseExpedisiAvailableSuccess(state, items) {
      state.listFWarehouseExpedisiAvailable = items;
    },
    loadFWarehouseExpedisiAvailableFailure(state) {
      state.listFWarehouseExpedisiAvailable = [];
    },
  },
  getters: {
    itemsSalesmanUsers(state) {
      return state.listFSalesmanUser;
    },
    itemsDivisionActive(state) {
      return state.listFDivision.filter((x) => x.statusActive === true);
    },
    itemsStoreActive(state) {
      return state.listFStore.filter((x) => x.statusActive === true);
    },
    itemsEkspedisiActive(state) {
      return state.listFStore.filter((x) => x.statusActive === true);
    },
    itemsWarehouseActive(state) {
      return state.listFWarehouse.filter((x) => x.statusActive === true);
    },
    itemsChannelActive(state) {
      return state.listFDistributionChannel.filter(
        (x) => x.statusActive === true
      );
    },
    itemsMethodPaymentByEkspedisi(state) {
      return state.listFPayMethodByEkspedisi;
    },
    itemsDiscount(state) {
      return state.listDiscount;
    },
    itemsCustomerGroupActive(state) {
      return state.listFCustomerGroup.filter((x) => x.statusActive === true);
    },
    getTempOrder(state) {
      return state.tempUserOrder;
    },
  },
};
