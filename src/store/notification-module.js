import FNotificationValidGroup from "../modules/top-notification/models/f-notification-group";
import FNotificationGroupService from "../modules/top-notification/services/f-notification-service";

export const notification = {
  namespaced: true,
  state: {
    itemValidNotificationGroup: new FNotificationValidGroup(),
    isSnackbar: false,
    message: null,
    itemsNotificationGroup: [],
    itemsNotificationGroupTemplate: [],
    itemNotificationLabelWithNotificationItem: [],
  },
  actions: {
    loadNotificationGroup({ commit }) {
      FNotificationGroupService.getAllFNotificationGroup().then(
        (response) => {
          commit("loadNotificationAllGroup", response.data);
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    },
    loadNotificationTemplateByGroupId({ commit }, payload) {
      FNotificationGroupService.getAllFNotificationTemplateByGroupId(
        payload.id
      ).then(
        (response) => {
          commit("loadNotificationTemplateByGroupId", response.data);
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    },
    loadNotificationLabelWithNotificationItem({ commit }) {
      FNotificationGroupService.getAllFNotificationLabelWithNotification().then(
        (response) => {
          commit(
            "loadNotificationLabelWithNotificationItem",
            response.data.map((e) => ({
              ...e,
              notificationList: e.notificationList.map((c) => ({
                ...c,
                more: false,
              })),
            }))
          );
        },
        (error) => {
          return Promise.reject(error);
        }
      );
    },
  },
  mutations: {
    snackbar(state, items) {
      state.isSnackbar = items;
    },
    message(state, items) {
      state.message = items;
    },
    loadNotificationAllGroup(state, items) {
      state.itemsNotificationGroup = items;
    },
    loadNotificationTemplateByGroupId(state, items) {
      state.itemsNotificationGroupTemplate = items;
    },
    loadNotificationLabelWithNotificationItem(state, items) {
      state.itemNotificationLabelWithNotificationItem = items;
    },
  },
};
