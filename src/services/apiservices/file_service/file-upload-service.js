import axios from "axios";
import ConstApiUrls from "../../const-api-urls";
import authHeaderMultipart from "../../../modules/auth/services/auth-header-multipart";

const API_URL = ConstApiUrls.API_SERVICE_URL;
const MICROSERVICE_URL = ConstApiUrls.FILE_SERVICE_URL;

class FileUploadService {
  uploadImage(file, onUploadProgress) {
    let formData = new FormData();
    formData.append("file", file);
    return axios.post(API_URL + "storage/upload_image", formData, {
      headers: authHeaderMultipart(),
      onUploadProgress,
    });
  }
  uploadDocument(file, onUploadProgress) {
    let formData = new FormData();
    formData.append("file", file);
    return axios.post(API_URL + "storage/upload_file", formData, {
      headers: authHeaderMultipart(),
      onUploadProgress,
    });
  }
  uploadImageMicroservice(
    file,
    onUploadProgress,
    model_id,
    folder_upload,
    isCompress
  ) {
    let formData = new FormData();
    formData.append("image[]", file);
    formData.append("folder_upload", folder_upload);
    formData.append("model_id", model_id);
    formData.append("isCompress", isCompress);
    return axios.post(MICROSERVICE_URL + "api/upload", formData, {
      headers: {
        "Access-Control-Allow-Credentials": "true",
        "Content-Type": "multipart/form-data",
      },
      onUploadProgress,
    });
  }
  deleteUploadImageByPathMicroservice(items) {
    return axios.delete(MICROSERVICE_URL + "api/upload", items);
  }
  deleteUploadImageByModelIdMicroservice(items) {
    return axios.delete(MICROSERVICE_URL + "api/deleteByModelId", items);
  }
  getUploadImageByModelIdMicroservice(modelId) {
    return axios.get(MICROSERVICE_URL + "api/findByModelId?query=" + modelId);
  }
}

export default new FileUploadService();
