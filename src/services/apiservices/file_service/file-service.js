import axios from "@/plugins/axios";
import ConstApiUrls from "../../const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;
const MICROSERVICE_URL = ConstApiUrls.FILE_SERVICE_URL;

class FileService {
  getApiUrl() {
    return API_URL;
  }
  image_url_verylow(file_name) {
    return API_URL + `storage/files_image_verylow/${file_name}`;
  }
  image_url_low(file_name) {
    return API_URL + `storage/files_image_low/${file_name}`;
  }
  image_url_medium(file_name) {
    return API_URL + `storage/files_image_medium/${file_name}`;
  }
  image_url_high(file_name) {
    return API_URL + `storage/files_image_high/${file_name}`;
  }
  image_url_original(file_name) {
    return API_URL + `storage/files_image_ori/${file_name}`;
  }
  deleteImage(file_name) {
    return axios.delete(API_URL + `storage/deleteimage/${file_name}`);
  }
  deleteImageMicroservice(file_name) {
    return axios.post(MICROSERVICE_URL + `api/deleteByPath`, {
      full_path: file_name,
    });
  }
  getImageMicroservice(file_name) {
    return axios.get(MICROSERVICE_URL + file_name);
  }
  deleteFile(file_name) {
    return axios.delete(API_URL + `storage/deletefiles/${file_name}`);
  }
  file_url(file_name) {
    return API_URL + `storage/files/${file_name}`;
  }
  getImageTest() {
    // return "http://localhost:8181/api/smartejakon/storage/files_image_medium/image_1631528731270.jpg";
    // return "http://localhost:8181/api/smartejakon/storage/files_image_verylow/image_kegiatan_1.jpg";
    return "http://localhost:8181/api/smartejakon/storage/files_image_medium/image_kegiatan_1.jpg";
  }
}
export default new FileService();
