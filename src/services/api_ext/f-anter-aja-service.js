import axios from "axios";
import authHeader from "../../modules/auth/services/auth-header";
import ConstApiUrls from "../../services/const-api-urls";
import RequestDest from "../../models/ext/request-dest";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FAnterAjaService {
  getApiUrl() {
    return API_URL;
  }

  getPostAnterAjaTarif(
    fwarehouseBean,
    fexpedisiBean,
    kabKota,
    kecamatan,
    weight
  ) {
    const requestDest = new RequestDest(
      fwarehouseBean,
      fexpedisiBean,
      kabKota,
      kecamatan,
      weight
    );
    return axios.post(API_URL + `getPostAnterAjaTarif`, requestDest, {
      headers: authHeader(),
    });
  }

  getAnterAjaTarifWithParams(
    fwarehouseBean,
    fexpedisiBean,
    kabKota,
    kecamatan,
    weight
  ) {
    // console.log(`${kabKota} >> ${kecamatan}`)
    return axios.get(
      API_URL +
        `getAnterAjaTarifWithParams?fwarehouseBean=${fwarehouseBean}&fexpedisiBean=${fexpedisiBean}&kabKota=${kabKota}&kecamatan=${kecamatan}&weight=${weight}`,
      { headers: authHeader() }
    );
  }

  getAnterAjaTarif(fwarehouseBean, fexpedisiBean, kabKota, kecamatan, weight) {
    return axios.get(
      API_URL +
        `getAnterAjaTarif/${fwarehouseBean}/${fexpedisiBean}/${kabKota}/${kecamatan}/${weight}`,
      { headers: authHeader() }
    );
  }

  getAnterAjaTarifBasic(fwarehouseBean, fexpedisiBean, destCode, weight) {
    return axios.get(
      API_URL +
        `getAnterAjaTarifBasic/${fwarehouseBean}/${fexpedisiBean}/${destCode}/${weight}`,
      { headers: authHeader() }
    );
  }

  getAnterAjaDestination() {
    return axios.get(API_URL + `getAnterAjaDestination`, {
      headers: authHeader(),
    });
  }

  getAnterAjaDestCode(fwarehouseBean, fexpedisiBean, kabKota, kecamatan) {
    return axios.get(
      API_URL +
        `getAnterAjaDestCode/${fwarehouseBean}/${fexpedisiBean}/${kabKota}/${kecamatan}`,
      { headers: authHeader() }
    );
  }
  fetchFromAnterAjaDestCodeApiAndSave(fdivisionBean, isSave) {
    return axios.get(
      API_URL + `getAnterAjaDestCodeAndSave/${fdivisionBean}/${isSave}`,
      { headers: authHeader() }
    );
  }

  getAnterAjaTracking(fwarehouseBean, fexpedisiBean, awbNumber) {
    return axios.get(
      API_URL +
        `getAnterAjaTracking/${fwarehouseBean}/${fexpedisiBean}/${awbNumber}`,
      { headers: authHeader() }
    );
  }
}
export default new FAnterAjaService();
