import axios from "axios";
import authHeader from "../../modules/auth/services/auth-header";
import ConstApiUrls from "../const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FAreaAnterAjaDestService {
  getApiUrl() {
    return API_URL;
  }

  getAllFAreaAnterAjaDest() {
    return axios.get(API_URL + `getAllFAreaAnterAjaDest`, {
      headers: authHeader(),
    });
  }
  getAllFAreaAnterAjaDestContaining(
    page,
    pageSize,
    sortBy,
    order,
    cityCode,
    cityName,
    districtCode,
    districtName
  ) {
    return axios.get(
      API_URL +
        `getAllFAreaAnterAjaDestContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&cityName=${cityName}&districtCode=${districtCode}&districtName=${districtName}`,
      { headers: authHeader() }
    );
  }
  getFAreaAnterAjaDestById(id) {
    return axios.get(API_URL + `getFAreaAnterAjaDestById/${id}`, {
      headers: authHeader(),
    });
  }
  updateFAreaAnterAjaDest(item) {
    return axios.put(API_URL + `updateFAreaAnterAjaDest/${item.id}`, item, {
      headers: authHeader(),
    });
  }
  createFAreaAnterAjaDest(item) {
    return axios.post(API_URL + `createFAreaAnterAjaDest`, item, {
      headers: authHeader(),
    });
  }
  createFAreaAnterAjaDestMultiple(items) {
    return axios.post(API_URL + `createFAreaAnterAjaDestMultiple`, items, {
      headers: authHeader(),
    });
  }

  deleteFAreaAnterAjaDest(id) {
    return axios.delete(API_URL + `deleteFAreaAnterAjaDest/${id}`, {
      headers: authHeader(),
    });
  }

  deleteAllFAreaAnterAjaDest(itemIds) {
    return axios.delete(API_URL + `deleteAllFAreaAnterAjaDest`, {
      headers: authHeader(),
      data: itemIds,
    });
  }
}
export default new FAreaAnterAjaDestService();
