import axios from "axios";
import authHeader from "../../modules/auth/services/auth-header";
import ConstApiUrls from "../../services/const-api-urls";
import RequestDest from "../../models/ext/request-dest";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FIdExpressService {
  getApiUrl() {
    return API_URL;
  }

  getPostIdExpressTarif(
    fwarehouseBean,
    fexpedisiBean,
    kabKota,
    kecamatan,
    weight,
    serviceDisplay
  ) {
    const requestDest = new RequestDest(
      fwarehouseBean,
      fexpedisiBean,
      kabKota,
      kecamatan,
      weight,
      serviceDisplay
    );

    // console.log(JSON.stringify(requestDest))

    return axios.post(API_URL + `getPostIdExpressTarif`, requestDest, {
      headers: authHeader(),
    });
  }

  getIdExpressTracking(fwarehouseBean, fexpedisiBean, awbNumber) {
    return axios.get(
      API_URL +
        `getIdExpressTracking/${fwarehouseBean}/${fexpedisiBean}/${awbNumber}`,
      { headers: authHeader() }
    );
  }

  fetchFromSiCepatDestCodeAndSave(fdivisionBean, isSave) {
    return axios.get(
      API_URL + `fetchFromSiCepatDestCodeAndSave/${fdivisionBean}/${isSave}`,
      { headers: authHeader() }
    );
  }
}
export default new FIdExpressService();
