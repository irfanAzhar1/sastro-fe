import axios from "axios";
import authHeader from "../../modules/auth/services/auth-header";
import ConstApiUrls from "../const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FSAPDestService {
  getApiUrl() {
    return API_URL;
  }

  getAllFAreaSAPDest() {
    return axios.get(API_URL + `getAllFAreaSAPDest`, { headers: authHeader() });
  }
  getAllFAreaSAPDestContaining(
    page,
    pageSize,
    sortBy,
    order,
    cityName,
    districtName,
    provinsiName
  ) {
    return axios.get(
      API_URL +
        `getAllFAreaSAPDestContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&cityName=${cityName}&districtName=${districtName}&provinceName=${provinsiName}`,
      { headers: authHeader() }
    );
  }
  fetchFromSAPDestCodeApiAndSave(divisionId) {
    return axios.get(
      API_URL + `saveFAreaSAPByEndpoint?fdivisionId=${divisionId}`,
      { headers: authHeader() }
    );
  }
  getFAreaSAPDestById(id) {
    return axios.get(API_URL + `getFAreaSAPDestById/${id}`, {
      headers: authHeader(),
    });
  }
  updateFAreaSAPDest(item) {
    return axios.put(API_URL + `updateFAreaSAPDest/${item.id}`, item, {
      headers: authHeader(),
    });
  }
  createFAreaSAPDest(item) {
    return axios.post(API_URL + `createFAreaSAPDest`, item, {
      headers: authHeader(),
    });
  }
  createFAreaSAPDestMultiple(items) {
    return axios.post(API_URL + `createFAreaSAPDestMultiple`, items, {
      headers: authHeader(),
    });
  }

  deleteFAreaSAPDest(id) {
    return axios.delete(API_URL + `deleteFAreaSAPDest/${id}`, {
      headers: authHeader(),
    });
  }

  deleteAllFAreaSAPDest(itemIds) {
    return axios.delete(API_URL + `deleteAllFAreaSAPDest`, {
      headers: authHeader(),
      data: itemIds,
    });
  }
}
export default new FSAPDestService();
