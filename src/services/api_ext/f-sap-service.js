import axios from "axios";
import authHeader from "../../modules/auth/services/auth-header";
import ConstApiUrls from "../const-api-urls";
import RequestDest from "../../models/ext/request-dest";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FSapService {
  getApiUrl() {
    return API_URL;
  }

  getPostSAPTarif(fwarehouseBean, fexpedisiBean, kabKota, kecamatan, weight) {
    const requestDest = new RequestDest(
      fwarehouseBean,
      fexpedisiBean,
      kabKota,
      kecamatan,
      weight
    );
    return axios.post(API_URL + `getPostSAPTarif`, requestDest, {
      headers: authHeader(),
    });
  }

  getSAPTarifWithParams(
    fwarehouseBean,
    fexpedisiBean,
    kabKota,
    kecamatan,
    weight
  ) {
    // console.log(`${kabKota} >> ${kecamatan}`)
    return axios.get(
      API_URL +
        `getSAPTarifWithParams?fwarehouseBean=${fwarehouseBean}&fexpedisiBean=${fexpedisiBean}&kabKota=${kabKota}&kecamatan=${kecamatan}&weight=${weight}`,
      { headers: authHeader() }
    );
  }

  getSAPTarif(fwarehouseBean, fexpedisiBean, kabKota, kecamatan, weight) {
    return axios.get(
      API_URL +
        `getSAPTarif/${fwarehouseBean}/${fexpedisiBean}/${kabKota}/${kecamatan}/${weight}`,
      { headers: authHeader() }
    );
  }

  getSAPTarifBasic(fwarehouseBean, fexpedisiBean, destCode, weight) {
    return axios.get(
      API_URL +
        `getSAPTarifBasic/${fwarehouseBean}/${fexpedisiBean}/${destCode}/${weight}`,
      { headers: authHeader() }
    );
  }

  getSAPDestination() {
    return axios.get(API_URL + `getSAPDestination`, { headers: authHeader() });
  }

  getSAPDestCode(fwarehouseBean, fexpedisiBean, kabKota, kecamatan) {
    return axios.get(
      API_URL +
        `getSAPDestCode/${fwarehouseBean}/${fexpedisiBean}/${kabKota}/${kecamatan}`,
      { headers: authHeader() }
    );
  }
  fetchFromSAPDestCodeApiAndSave(fdivisionBean, isSave) {
    return axios.get(
      API_URL + `getSAPDestCodeAndSave/${fdivisionBean}/${isSave}`,
      { headers: authHeader() }
    );
  }

  getSAPTracking(fwarehouseBean, fexpedisiBean, awbNumber) {
    return axios.get(
      API_URL +
        `getSAPTracking/${fwarehouseBean}/${fexpedisiBean}/${awbNumber}`,
      { headers: authHeader() }
    );
  }
}
export default new FSapService();
