import axios from "@/plugins/axios";
import ConstApiUrls from "../services/const-api-urls";

const API_URL = ConstApiUrls.TEST_SERVICE_URL;

class TestService {
  getApiUrl() {
    return API_URL;
  }

  getPublicContent() {
    return axios.get(API_URL + "all");
  }
  getUserBoard() {
    return axios.get(API_URL + "user");
  }

  getModeratorBoard() {
    return axios.get(API_URL + "mod");
  }

  getAdminBoard() {
    return axios.get(API_URL + "admin");
  }
}

export default new TestService();
