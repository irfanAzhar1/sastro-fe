const ETrackingSAPStatus = Object.freeze({
    PENDING_PIKCUP: 'ENTRI (PENDING PICKUP)',
    RE_PIKCUP: 'ENTRI (SEDANG PICKUP ULANG)',
    ON_PIKCUP: 'ENTRI (SEDANG DI PICKUP)',
    PICKUP: 'PICKED UP',
    VERIFIED: 'ENTRI VERIFIED',
    OUTGOING: 'MANIFEST OUTGOING',
    INCOMING: 'INCOMING',
    DELIVERY: 'DELIVERY',
    DELIVERYDOUBLECASE: 'DELIVERY ',
    UNDELIVERED: 'POD - UNDELIVERED',
    DELIVERED: 'POD - DELIVERED',
    OUTGOING_SMU: 'OUTGOING SMU',
    HANDOVER_COURIER:'HANDOVER COURIER',
    INCOMING_SMU: 'INCOMING SMU',
    INCOMING_TRANSIT: 'INCOMING TRANSIT',
    OUTGOING_TRANSIT: 'OUTGOING TRANSIT',
    DELIVERY_RETURN: 'DELIVERY RETURN'
})

const ETrackingSAPStatusList = Object.freeze([
    {
        row_state: ETrackingSAPStatus.ON_PIKCUP,
        description: 'Sedang di pickup',
    },
    {
        row_state: ETrackingSAPStatus.PENDING_PIKCUP,
        description: 'Proses Pick Up Ditunda',
    },
    {
        row_state: ETrackingSAPStatus.RE_PIKCUP,
        description: 'Proses Pick Up Ulang',
    },
    {
        row_state: ETrackingSAPStatus.PICKUP,
        description: 'Paket telah di pickup',
    },
    {
        row_state: ETrackingSAPStatus.VERIFIED,
        description: 'Verifikasi paket',
    },
    {
        row_state: ETrackingSAPStatus.OUTGOING,
        description: 'Keluar dari ',
    },
    {
        row_state: ETrackingSAPStatus.INCOMING,
        description: 'Sampai di ',
    },
    {
        row_state: ETrackingSAPStatus.OUTGOING_SMU,
        description: 'Dalam Perjalanan ke Cabang  ',
    },
    {
        row_state: ETrackingSAPStatus.DELIVERY, // double case
        description: 'Paket Diantar Kurir   ',
    },
    {
        row_state: ETrackingSAPStatus.DELIVERYDOUBLECASE, // double case
        description: 'Paket Diantar Kurir   ',
    },
    {
        row_state: ETrackingSAPStatus.UNDELIVERED,
        description: 'Paket Belum Berhasil Terkirim',
    },
    {
        row_state: ETrackingSAPStatus.DELIVERED,
        description: 'Paket Telah Diterima Oleh ',
    },
    {
        row_state: ETrackingSAPStatus.HANDOVER_COURIER,
        description: 'Serah Terima Paket dari Kurir ke Cabang ',
    },
    {
        row_state: ETrackingSAPStatus.INCOMING_SMU,
        description: 'Incoming Smu',
    },
    {
        row_state: ETrackingSAPStatus.INCOMING_TRANSIT,
        description: 'Incoming Transit',
    },
    {
        row_state: ETrackingSAPStatus.OUTGOING_TRANSIT,
        description: 'Outgoing Transit',
    },
    {
        row_state: ETrackingSAPStatus.DELIVERY_RETURN,
        description: 'Paket Diantar Kurir untuk Dikembalikan'
    }
])

export {ETrackingSAPStatus as default, ETrackingSAPStatusList};