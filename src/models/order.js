export default class Order {
  constructor(
    division_id = null,
    salesman_id = null,
    store_id = null,
    warehouse_id = null,
    customer_id = null,
    is_admin_free = false,
    sub_area_id = null,

    customer_name = null,
    customer_address = null,
    customer_phone = null,
    customer_state = null,
    customer_city = null,
    customer_district = null,

    sub_total = 0,
    ongkir = 0,
    discount = 0,
    discountManual = 0,
    admin_fee = 0,
    total = 0,
    status = 0,

    invoice_no = null,
    order_no = null,
    waybill_no = null,
    ekspedisi_code = null,
    notes = null,
    ekspedisi_id = null,
    payment_id = null,
    discount_id = null,
    items = [],
    created = null
  ) {
    this.division_id = division_id;
    this.salesman_id = salesman_id;
    this.store_id = store_id;
    this.warehouse_id = warehouse_id;
    this.customer_id = customer_id;
    this.is_admin_free = is_admin_free;
    this.sub_area_id = sub_area_id;

    this.customer_name = customer_name;
    this.customer_address = customer_address;
    this.customer_phone = customer_phone;
    this.customer_state = customer_state;
    this.customer_city = customer_city;
    this.customer_district = customer_district;

    this.sub_total = sub_total;
    this.ongkir = ongkir;
    this.admin_fee = admin_fee;
    this.discountManual = discountManual;
    this.total = total;

    this.invoice_no = invoice_no;
    this.order_no = order_no;
    this.waybill_no = waybill_no;
    this.ekspedisi_code = ekspedisi_code;
    this.notes = notes;
    this.ekspedisi_id = ekspedisi_id;
    this.payment_id = payment_id;
    this.discount_id = discount_id;
    this.items = items;
    this.created = created;
  }
}
