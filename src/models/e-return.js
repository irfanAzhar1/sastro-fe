const EReturn = Object.freeze({
   REUSE:1,
   REPACK:2,
   REJECT:3,
   NOT_MATCH:4
})

const EReturns = Object.freeze([
   {
       id: EReturn.REUSE,
       description: 'Reuse',
       shortName: 'REUSE'
   },
   {
       id: EReturn.REPACK,
       description: 'Repack',
       shortName: 'REPACK'
   },
   {
       id: EReturn.REJECT,
       description: 'Reject',
       shortName: 'REJECT'
   },
   {
        id: EReturn.NOT_MATCH,
        description: 'Not match',
        shortName: 'NOT MATCH'
   },
])
export {EReturn as default, EReturns}
