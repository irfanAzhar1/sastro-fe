export default class FAreaAnterAjaDest {
  constructor(
    id=0,
    fdivisionBean=0,

    province="",
    cityName="",
    districtCode="",
    districtName="",

    created,
    modified,
    modifiedBy

  ) {
    this.id = id;
    this.fdivisionBean = fdivisionBean;

    this.province = province;
    this.cityName = cityName;
    this.districtCode = districtCode;
    this.districtName = districtName;

    this.created = created;
    this.modified = modified;
    this.modifiedBy = modifiedBy;
  }

}
