export default class FAreaSAP {
   constructor(
     id,
     fdivisionBean,
 
     cityCode,
     cityName,
 
     districtCode,
     districtName,
 
     provinsiCode,
     provinsiName,
 
     tlcBranchCode,
     zoneCode,

     created,
     modifiedBy
   ) {
     this.id = id;
     this.fdivisionBean = fdivisionBean;
 
     this.cityCode = cityCode;
     this.cityName = cityName;

     this.districtCode = districtCode;
     this.districtName = districtName;

     this.provinsiCode = provinsiCode;
     this.provinsiName = provinsiName;
 
     this.tlcBranchCode = tlcBranchCode;
     this.zoneCode = zoneCode;
 
     this.created = created;
     this.modifiedBy = modifiedBy;
   }
 
 }
 