export default class FAreaIdExpressDest {
  constructor(
    id=0,
    fdivisionBean=0,

    stateCode="",
    stateName="",
    cityCode="",
    cityName="",
    districtCode="",
    districtName="",

    created,
    modified,
    modifiedBy

  ) {
    this.id = id;
    this.fdivisionBean = fdivisionBean;

    this.stateCode = stateCode;
    this.stateName = stateName;
    this.cityCode = cityCode;
    this.cityName = cityName;
    this.districtCode = districtCode;
    this.districtName = districtName;

    this.created = created;
    this.modified = modified;
    this.modifiedBy = modifiedBy;
  }

}
