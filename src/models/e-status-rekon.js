
const EStatusRekon = Object.freeze({
    PAY_IN: 0,
    PAY_OUT: 1
})

const EStatusRekons= Object.freeze([
    {
        id: EStatusRekon.PAY_IN,
        description: 'Expedisi telah membayar ke PT',
        shortDesc: 'PAY IN',
        color: 'grey'
    },
    {
        id: EStatusRekon.PAY_OUT,
        description: 'PT Sudah membayar ke dealer',
        shortDesc: 'PAY OUT',
        color: 'orange'
    }
])

export  {EStatusRekon as default , EStatusRekons}