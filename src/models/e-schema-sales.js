
const ESchemaSalesdetail = Object.freeze([
    {
        column: 'Nomor',
        type: Number,
        value: item => item.id
    },
    {
        column: 'OrderNo',
        type: String,
        value: item => item.orderno
    },
    {
        column: 'Tanggal Order',
        type: Date,
        format: 'dd-MMM-yyyy',
        value: item => new Date(item.orderDate)
    },
    {
        column: 'InvoiceNo',
        type: String,
        width:20,
        value: item => item.invoiceno
    },
    {
        column: 'Tgl Invoice',
        type: Date,
        format: 'dd-MMM-yyyy',
        value: item => new Date(item.invoiceDate)
    },
    {
        column: 'DeliveryNumber',
        type: String,
        width:20,
        value: item => item.deliveryNumber
    },
    {
        column: 'Tanggal Delivery',
        type: Date,
        format: 'dd-MMM-yyyy',
        value: item => new Date(item.deliveryDate)
    },
    {
        column: 'Warehouse',
        type: String,
        width:15,
        value: item => item.fwarehouse.kode1
    },
    {
        column: 'Expedisi',
        type: String,
        width:15,
        value: item => item.fexpedisi.kode1
    },
    {
        column: 'Status Pengiriman',
        type: String,
        width:20,
        value: item => item.statusKirim
    },
    {
        column: 'Salesman',
        type: String,
        width:25,
        value: item => item.fsalesman.spname
    },
    {
        column: 'Distributor',
        type: String,
        width:25,
        value: item => item.fdivision.description
    },
    {
        column: 'Nama Toko',
        type: String,
        width:25,
        value: item => item.fstore.description
    },
    {
        column: 'Kode Member',
        type: String,
        value: item => item.fcustomer.custno
    },
    {
        column: 'Member/Customer',
        type: String,
        width:25,
        value: item => item.fcustomer.custname
    },
    {
        column: 'Nama Penerima',
        type: String,
        width:20,
        value: item => item.destName
    },
    {
        column: 'Nomor HP',
        type: Number,
        width:15,
        value: item => item.destPhone
    },
    {
        column: 'Alamat Penerima',
        type: String,
        width:30,
        value: item => item.destAddress1
    },
    {
        column: 'Kecamatan Penerima',
        type: String,
        width:25,
        value: item => item.destCity1
    },
    {
        column: 'Propinsi Penerima',
        type: String,
        width:25,
        value: item => item.destState1
    },
    {
        column: 'Kode Tujuan(Expedisi)',
        type: String,
        width:25,
        value: item => item.destCode
    },
    {
        column: 'Items',
        type: String,
        width:25,
        value: item => item.items
    },

    {
        column: 'Total Items AfterDisc',
        type: Number,
        format: '#,##0',
        value: item => item.amountAfterDiscPlusRpAfterPpn_FG
    },
    {
      column: 'Penambahan',
      type: Number,
      format:"#,##0",
      value: item => item.penambahanRp
    },
    {
        column: 'Ongkir',
        type: Number,
        format: '#,##0',
        value: item => item.ongkirRp
    },
    {
        column: 'Penanganan COD',
        type: Number,
        format: '#,##0',
        value: item => item.byPenanganan
    },
    {
        column: 'Uang Masuk',
        type: Number,
        format: '#,##0',
        value: item => item.totalBayar
    },
    {
      column: 'Penambahan',
      type: Number,
      format: '#,##0',
      value: item => item.penambahan
    },
])


export default ESchemaSalesdetail
