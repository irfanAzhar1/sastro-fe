export default class FReturnFilter {
    constructor(
        id = 0,
        pageNo= 0,
        pageSize = 0,
        sortBy="id",
        order="DESC",
        search="",
        fwarehouseIds = [],
        fsalesmanIds = [],
        freturnStatus = [],
        retunDateFrom= new Date(),
        returnDateTo = new Date(),
    ) {
      this.id = id;
      this.pageNo = pageNo;
      this.pageSize = pageSize;
      this.sortBy = sortBy;
      this.order = order;
      this.search = search;
  
      this.fwarehouseIds = fwarehouseIds;
      this.fsalesmanIds = fsalesmanIds;
      this.freturnStatus = freturnStatus;
      
      this.retunDateFrom = retunDateFrom;
      this.returnDateTo = returnDateTo;
    }
  
  }
  