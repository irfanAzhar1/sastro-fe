export default class FRekonFilter {
    constructor(
        id = 0,
        pageNo= 0,
        pageSize = 0,
        sortBy="id",
        order="DESC",
        search="",
        fwarehouseIds = [],
        fsalesmanIds = [],
        status = [],
        dateFrom= new Date(),
        dateTo = new Date(),
    ) {
      this.id = id;
      this.pageNo = pageNo;
      this.pageSize = pageSize;
      this.sortBy = sortBy;
      this.order = order;
      this.search = search;
  
      this.fwarehouseIds = fwarehouseIds;
      this.fsalesmanIds = fsalesmanIds;
      this.status = status;
      
      this.dateFrom = dateFrom;
      this.dateTo = dateTo;
    }
  
  }
  