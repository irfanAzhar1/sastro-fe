export default class FTicketingFilter {
  constructor(
    page = 0,
    pageSize = 0,
    sortBy = "",
    order = "",
    deliveryNumber = "",
    categoryId = "",
    status = "",
    expedisiIds = [],
    dateEnd = "",
    dateStart = "",
    isEkspedisi = ""

  ){
    this.page = page;
    this.pageSize = pageSize;
    this.sortBy = sortBy;
    this.order = order;
    this.deliveryNumber = deliveryNumber;
    this.categoryId = categoryId;
    this.status = status
    this.expedisiIds = expedisiIds
    this.dateEnd = dateEnd
    this.dateStart = dateStart
    this.isEkspedisi = isEkspedisi
  }
}
