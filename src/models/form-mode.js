const FormMode = Object.freeze({
    EDIT_FORM: 'EDIT_FORM',
    NEW_FORM: 'NEW_FORM',
    EDIT_DETIL: 'EDIT_DETIL',
    NEW_DETIL: 'NEW_DETIL'
})
export default FormMode