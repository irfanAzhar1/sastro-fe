export default class FTax {
  constructor(
    id,
    kode1,
    description,
    fdivisionBean,
    statusActive=true,

    taxPercent,

    created,
    modified,
    modifiedBy
  ) {
    this.id = id;
    this.kode1 = kode1;
    this.description = description;
    this.taxPercent =  taxPercent;
    this.statusActive = statusActive;
    this.fdivisionBean = fdivisionBean;
    this.created = created;
    this.modified = modified;
    this.modifiedBy = modifiedBy;
  }

}
