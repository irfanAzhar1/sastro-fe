export default class FCustomerFile {
  constructor(
    id,
    fcustomerBean,
    fileName,

    title,
    description,

    fileType,
    parentType,
    flag,
    uploadFrom,

    created,
    modifiedBy
  ) {
    this.id = id;
    this.fcustomerBean = fcustomerBean;
    this.fileName = fileName;

    this.title = title;
    this.description = description;

    this.fileType = fileType;
    this.parentType = parentType;
    this.flag = flag;
    this.uploadFrom = uploadFrom;

    this.created = created;
    this.modifiedBy = modifiedBy;
  }

}
