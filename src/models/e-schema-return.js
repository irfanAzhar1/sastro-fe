const ESchemaReturn = Object.freeze([
    {
        column: 'DeliveryNumber',
        type: String,
        value: item => item.DeliveryNumber
    },
    {
        column: 'Tanggal Return',
        type: Date,
        format: 'dd-MMM-yyyy',
        value: item => new Date(item.returnDate)
    },
    {
        column: 'Tanggal Invoice',
        type: Date,
        format: 'dd-MMM-yyyy',
        value: item => new Date(item.invoiceDate)
    },
    {
        column: 'Courier',
        type: String,
        value: item => item.courier
    },
    {
        column: 'Distributor',
        type: String,
        value: item => item.distributor
    },
    {
        column: 'Salesman',
        type: String,
        value: item => item.salesman
    },
    {
        column: 'ShopName',
        type: String,
        value: item => item.ShopName
    },
    {
        column: 'Items',
        type: String,
        value: item => item.items
    },
    {
      column: 'Kode',
      type: String,
      value: item => item.pcode
    },
    {
      column: 'Kode warehouse',
      type: String,
      value: item => item.warehouseCode
    },
    {
        column: 'Item',
        type: String,
        value: item => item.item
    },
    {
        column: 'Status barang',
        type: String,
        value: item => item.statusBarang
    },
    {
        column: 'Biaya return',
        type: String,
        value: item => item.biayarReturn
    },
    {
        column: 'Status',
        type: String,
        value: item => item.statusReturn
    },
    {
        column: 'Created At',
        type: Date,
        format: 'dd-MMM-yyyy hh:mm:ss',
        value: item => item.createdAt
    },
    {
        column: 'Modified Status',
        type: String,
        value: item => item.modifiedStatus
    },
])


export default ESchemaReturn
