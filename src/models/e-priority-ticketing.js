const EPriority = Object.freeze({
  LOW:"LOW",
  MEDIUM:"MEDIUM",
  HIGH:"HIGH"
})

const EPriorities = Object.freeze([
  {
    id: EPriority.LOW,
    description: 'Reuse',
    shortName: 'REUSE'
  },
  {
    id: EPriority.MEDIUM,
    description: 'Repack',
    shortName: 'REPACK'
  },
  {
    id: EPriority.HIGH,
    description: 'Reject',
    shortName: 'REJECT'
  },
])
export {EPriority as default, EPriorities}
