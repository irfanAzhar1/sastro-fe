
const ESchemaSalesDetailSimple = Object.freeze([
  {
    column: 'Nomor',
    type: Number,
    value: item => item.id
  },
  {
    column: 'OrderNo',
    type: String,
    value: item => item.orderno
  },
  {
    column: 'Tanggal Order',
    type: Date,
    format: 'dd-MMM-yyyy',
    value: item => new Date(item.orderDate)
  },
  {
    column: 'InvoiceNo',
    type: String,
    width:20,
    value: item => item.invoiceno
  },
  {
    column: 'Tgl Invoice',
    type: Date,
    format: 'dd-MMM-yyyy',
    value: item => new Date(item.invoiceDate)
  },
  {
    column: 'DeliveryNumber',
    type: String,
    width:20,
    value: item => item.deliveryNumber
  },
  {
    column: 'Tanggal Delivery',
    type: Date,
    format: 'dd-MMM-yyyy',
    value: item => new Date(item.deliveryDate)
  },
  {
    column: 'Warehouse',
    type: String,
    width:15,
    value: item => item.fwarehouse.kode1
  },
  {
    column: 'Expedisi',
    type: String,
    width:15,
    value: item => item.fexpedisi.kode1
  },
  {
    column: 'Status Pengiriman',
    type: String,
    width:20,
    value: item => item.statusKirim
  },
  {
    column: 'Kode Distributor',
    type: String,
    width:25,
    value: item => item.fdivision.kode1
  },
  {
    column: 'Distributor',
    type: String,
    width:25,
    value: item => item.fdivision.description
  },
  {
    column: 'Kode Barang',
    type: String,
    width:15,
    value: item => item.pcode
  },
  {
    column: 'Nama Barang',
    type: String,
    width:25,
    value: item => item.pname
  },
  {
    column: 'Qty',
    type: Number,
    width:10,
    value: item => item.qty
  },
  {
    column: 'Ongkir',
    type: Number,
    format: '#,##0',
    value: item => item.ongkirRp
  },
])


export default ESchemaSalesDetailSimple
