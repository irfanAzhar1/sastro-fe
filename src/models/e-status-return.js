const EStatusReturn = Object.freeze({
  PENDING: 1,
  INVOICE: 2,
  REFUND: 3,
});

const EStatusReturns = Object.freeze([
  {
    id: EStatusReturn.PENDING,
    description: "Menunggu Konfirmasi",
    shortDesc: "Pending",
    color: "grey",
  },
  {
    id: EStatusReturn.INVOICE,
    description: "PT Sudah memberikan invoice",
    shortDesc: "Invoice",
    color: "orange",
  },

  {
    id: EStatusReturn.REFUND,
    description: "PT Telah melakukan refund",
    shortDesc: "Refund",
    color: "blue",
  },
]);

export { EStatusReturn as default, EStatusReturns };
