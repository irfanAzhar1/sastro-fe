export default class FExpedisi {
  constructor(
    id,
    kode1="",
    kode2="", //Kode service
    description="",
    avatarImage="",
    fdivisionBean,
    statusActive=true,

    contactName="",

    address1="",
    address2="",
    district1="",
    city1="",
    state1="",
    phone,
    countryCode=62,

    zipCode,
    email="",

    codPersen,
    ongkirPersen,
    penangananPersen,

    created,
    modified,
    modifiedBy
  ) {
    this.id = id;
    this.kode1 = kode1;
    this.kode2 = kode2;
    this.description = description;
    this.avatarImage = avatarImage;
    this.statusActive = statusActive;
    this.fdivisionBean = fdivisionBean;

    this.contactName = contactName;
    this.address1 = address1;
    this.address2 = address2;
    this.district1 = district1;
    this.city1 = city1;
    this.state1 = state1;
    this.phone = phone;
    this.countryCode = countryCode;

    this.zipCode =zipCode;
    this.email  =email;

    this.codPersen = codPersen;
    this.ongkirPersen = ongkirPersen;
    this.penangananPersen = penangananPersen;

    this.created = created;
    this.modified = modified;
    this.modifiedBy = modifiedBy;
  }

}
