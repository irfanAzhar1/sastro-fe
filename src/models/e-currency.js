
const ECurrency = Object.freeze({
    IDR: "IDR",
    USD: "USD",
    EUR: "EUR",
    JPY: "JPY",
    SGD: "SGD",
    MYR: "MYR",
    THP: "THB",
    CNH: "CNH",
    HKD: "HKD",
    TWD: "TWD",
    OTH: "OTH"
})
export default ECurrency