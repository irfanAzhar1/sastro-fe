export default class FSalesTicket {
  constructor(
    id = 0,
    deliveryNumber = "",
    receiverName = "",
    phoneNumber = "",
    codPrice = 0,
    amountAfterDiscPlusRpAfterPpn_FG  = 0,
    storeName = "",
    subArea = "",
    expedisiName = "",
    expedisiCity = "",
    expedisiDisctrict = "",
    expedisiAddress = "",
    deliveryStatusCode = "",
    deliveryStatusDescription = "",
    remark = "",
    destCode = "",
    originCode = ""
    ) {
      this.id = id
      this.deliveryNumber = deliveryNumber
      this.receiverName = receiverName
      this.phoneNumber = phoneNumber
      this.codPrice = codPrice
      this.amountAfterDiscPlusRpAfterPpn_FG = amountAfterDiscPlusRpAfterPpn_FG
      this. storeName = storeName
      this.subArea = subArea
      this.expedisiName = expedisiName
      this.expedisiCity = expedisiCity
      this.expedisiDisctrict = expedisiDisctrict
      this.expedisiAddress = expedisiAddress
      this.deliveryStatusCode = deliveryStatusCode
      this.deliveryStatusDescription = deliveryStatusDescription
      this.remark = remark
      this.destCode = destCode
      this.originCode = originCode
    }


}
