import Vue from "vue";
import VueRouter from "vue-router";
import LandingPage from "../modules/public/views/LandingPage.vue";
import ERole from "../models/e-role";
import store from "../store/index";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: LandingPage,
    meta: {
      requiredAuth: false,
    },
  },
  {
    path: "/dashboard",
    name: "DashboardUtama",
    component: () => import("../modules/dashboard/views/Dashboard.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_SALES,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_INVENTORY,
        ERole.ROLE_AGEN,
        ERole.ROLE_RSL_1,
        ERole.ROLE_CS,
      ],
    },
  },
  {
    path: "/notification",
    name: "Notification",
    component: () => import("../modules/notification/views/Notification.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_USER,
        ERole.ROLE_USER1,
        ERole.ROLE_SALES,
        ERole.ROLE_ACCOUNT,
        ERole.ROLE_FINANCE,
        ERole.ROLE_HUTANG,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_PIUTANG_1,
        ERole.ROLE_PURCHASE,
        ERole.ROLE_INVENTORY,
        ERole.ROLE_AGEN,
        ERole.ROLE_RSL_1,
        ERole.ROLE_CS,
        ERole.ROLE_EKSPEDISI,
      ],
    },
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../modules/auth/views/Login.vue"),
  },
  // {
  //     path: '/register',
  //     name: 'Register',
  //     component: () =>
  //         import ('../views/Register.vue')
  // },
  {
    path: "/change-profile",
    name: "ChangeProfile",
    component: () => import("../modules/admin/views/ChangeProfile.vue"),
    meta: {
      requiredAuth: true,
    },
  },
  {
    path: "/profile",
    name: "Profile",
    component: () => import("../modules/auth/views/Profile.vue"),
    meta: {
      requiredAuth: true,
    },
  },
  {
    path: "/admin-sistem",
    name: "AdminSistem",
    component: () => import("../modules/admin/views/SettingAndUser.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1],
    },
  },
  {
    path: "/discount",
    name: "Discount",
    component: () => import("../modules/discount/views/Discount.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1],
    },
  },
  {
    path: "/admin-notification-buttom",
    name: "AdminNotificationButtom",
    component: () =>
      import("../modules/bottom-notification/views/ButtomNotification.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/admin-notification-top",
    name: "AdminNotificationTop",
    component: () =>
      import("../modules/top-notification/views/TopNotification.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/admin-struktur-organisasi",
    name: "StrukturOrganisasi",
    component: () => import("../modules/admin/views/StrukturOrganisasi.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_USER,
      ],
    },
  },
  {
    path: "/customer",
    name: "Customer",
    component: () => import("../modules/mitra/views/Customer.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_CS,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_AGEN,
        ERole.ROLE_RSL_1,
        ERole.ROLE_SALES,
      ],
    },
  },
  {
    path: "/cust-category",
    name: "CustCateg",
    component: () => import("../modules/mitra/views/CustCateg.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/cust-region",
    name: "CustRegion",
    component: () => import("../modules/mitra/views/CustRegion.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/supplier",
    name: "Supplier",
    component: () => import("../modules/mitra/views/Vendor.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/expedisi",
    name: "Expedisi",
    component: () => import("../modules/mitra/views/Expedisi.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/mp-store",
    name: "Store",
    component: () => import("../modules/mitra/views/MpStore.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_AGEN,
      ],
    },
  },
  {
    path: "/product",
    name: "Product",
    component: () => import("../modules/material/views/ProductView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_USER,
        ERole.ROLE_SALES,
      ],
    },
  },
  {
    path: "/product-category",
    name: "ProductCateg",
    component: () => import("../modules/material/views/ProductCategView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/product-sales-brand",
    name: "ProductSalesBrand",
    component: () => import("../modules/material/views/SalesBrandView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/product-ar-payment",
    name: "ProductTax",
    component: () => import("../modules/material/views/ProductTaxView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/warehouse",
    name: "Warehouse",
    component: () => import("../modules/inventory/views/Warehouse.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_USER,
      ],
    },
  },
  {
    path: "/return",
    name: "Return",
    component: () => import("../modules/inventory/views/ReturnData.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_USER,
        ERole.ROLE_SALES,
        ERole.ROLE_AGEN,
        ERole.ROLE_RSL_1,
        ERole.ROLE_INVENTORY,
        ERole.ROLE_INVENTORY_1,
        ERole.ROLE_INVENTORY_2,
        ERole.ROLE_PIUTANG,
      ],
    },
  },
  {
    path: "/rekonsiliasi",
    name: "Rekonsiliasi",
    component: () => import("../modules/inventory/views/Rekonsiliasi.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_INVENTORY,
        ERole.ROLE_RSL_1,
        ERole.ROLE_AGEN,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_SALES,
        ERole.ROLE_CS,
      ],
    },
  },
  {
    path: "/general-config",
    name: "General Config",
    component: () =>
      import("../modules/general-config/views/GeneralConfig.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/sales",
    name: "SalesInvoice",
    component: () => import("../modules/sales/views/SalesInvoiceView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_INVENTORY,
        ERole.ROLE_RSL_1,
        ERole.ROLE_AGEN,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_SALES,
        ERole.ROLE_CS,
      ],
    },
  },
  {
    path: "/sales/:statusPengiriman",
    name: "SalesInvoiceStatusPengiriman",
    component: () => import("../modules/sales/views/SalesInvoiceView.vue"),
    meta: {
      requiredAuth: true,
    },
  },
  {
    path: "/sales/:statusPengiriman/:dateFrom/:dateTo",
    name: "SalesInvoiceStatusPengirimanDeliveryRange",
    component: () => import("../modules/sales/views/SalesInvoiceView.vue"),
    meta: {
      requiredAuth: true,
    },
  },
  {
    path: "/sales-delivery",
    name: "SalesDelivery",
    component: () => import("../modules/sales/views/SalesDeliveryView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_INVENTORY, ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1],
    },
  },
  {
    path: "/sales-return-management",
    name: "SalesReturnManagement",
    component: () => import("../modules/sales/views/ReturnManagementView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_INVENTORY, ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1],
    },
  },
  {
    path: "/sales-print",
    name: "PrintResi",
    component: () => import("../modules/sales/views/PrintResiView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_INVENTORY, ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1],
    },
  },
  {
    path: "/scan-resi",
    name: "ScanResi",
    component: () => import("../modules/sales/views/ScanResiView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_INVENTORY, ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1],
    },
  },
  {
    path: "/pay-method",
    name: "PayMethod",
    component: () => import("../modules/paymethod/views/PayMethodView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_USER,
      ],
    },
  },

  {
    path: "/pembayaran-ar-invoice",
    name: "PaymentArInvoice",
    component: () => import("../modules/piutang/views/PaymentArInvoice.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_USER,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_PIUTANG_1,
      ],
    },
  },
  {
    path: "/pembayaran-ar",
    name: "ArPaymentView",
    component: () => import("../modules/piutang/views/ArPaymentView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_USER,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_PIUTANG_1,
      ],
    },
  },
  {
    path: "/setting-promosi",
    name: "SettingPromosi",
    component: () => import("../modules/promotion/views/SettingPromosi.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/report-komisi",
    name: "ReportKomisi",
    component: () => import("../modules/laporan/views/KomisiView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_AGEN,
        ERole.ROLE_RSL_1,
        ERole.ROLE_CS,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_SALES,
      ],
    },
  },
  {
    path: "/reports",
    name: "Reports",
    component: () => import("../modules/laporan/views/ReportsView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_AGEN,
        ERole.ROLE_RSL_1,
        ERole.ROLE_CS,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_SALES,
      ],
    },
  },
  {
    path: "/reports-analisis",
    name: "AnalisisView",
    component: () => import("../modules/laporan/views/AnalisisView.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/sales-export",
    name: "reportSales",
    component: () => import("../modules/report/views/reportSales.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_INVENTORY,
        ERole.ROLE_RSL_1,
        ERole.ROLE_AGEN,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_SALES,
        ERole.ROLE_CS,
      ],
    },
  },
  {
    path: "/updateStatusSales",
    name: "updateStatusSales",
    component: () => import("../views/updateStatusSales.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/catalog-product",
    name: "catalogProduct",
    component: () =>
      import("../modules/catalog-product/views/CatalogProduct.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_USER,
        ERole.ROLE_USER1,
        ERole.ROLE_SALES,
        ERole.ROLE_ACCOUNT,
        ERole.ROLE_FINANCE,
        ERole.ROLE_HUTANG,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_PIUTANG_1,
        ERole.ROLE_INVENTORY,
        ERole.ROLE_AGEN,
        ERole.ROLE_RSL_1,
        ERole.ROLE_CS,
      ],
    },
  },
  {
    path: "/ticket-category",
    name: "ticketCategory",
    component: () =>
      import("../modules/ticketing/views/SupportTicketCategory.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/tickets",
    name: "ticketList",
    component: () => import("../modules/ticketing/views/SupportTicket.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_USER,
        ERole.ROLE_SALES,
        ERole.ROLE_AGEN,
        ERole.ROLE_RSL_1,
        ERole.ROLE_CS,
        ERole.ROLE_EKSPEDISI,
      ],
    },
  },
  {
    path: "/ticket-master-status",
    name: "TicketMasterStatus",
    component: () =>
      import("../modules/ticketing/views/MasterTicketStatus.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },
  {
    path: "/feedback",
    name: "feedback",
    component: () => import("../modules/feedback/views/Feedback.vue"),
    meta: {
      requiredAuth: true,
      authorize: [
        ERole.ROLE_ADMIN,
        ERole.ROLE_ADMIN1,
        ERole.ROLE_ADMIN2,
        ERole.ROLE_SALES,
        ERole.ROLE_PIUTANG,
        ERole.ROLE_AGEN,
        ERole.ROLE_RSL_1,
        ERole.ROLE_CS,
      ],
    },
  },
  {
    path: "/banner-setting",
    name: "bannerSetting",
    component: () => import("../modules/banner/views/BannerSetting.vue"),
    meta: {
      requiredAuth: true,
      authorize: [ERole.ROLE_ADMIN, ERole.ROLE_ADMIN1, ERole.ROLE_ADMIN2],
    },
  },

  {
    path: "/term_condition",
    name: "termCondition",
    component: () => import("../modules/public/views/Tnc.vue"),
    meta: {
      requiredAuth: false,
    },
  },
  {
    path: "*",
    name: "NotFoundPage",
    component: () => import("../modules/public/views/NotFoundPage.vue"),
    meta: {
      requiredAuth: false,
    },
  },
  {
    path: "/forbidden",
    name: "Forbidden",
    component: () => import("../modules/public/views/ForbiddenPage.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: import.meta.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const { authorize } = to.meta;

  /* this for background landing page and admin page */
  if (to.path === "/forbidden" || to.name === "NotFoundPage") {
    store.dispatch("data/loadBackground", "#FFF");
  } else {
    store.dispatch("data/loadBackground", "#EEE");
  }

  /* required auth route */
  if (store.state.auth.user === null) {
    if (to.meta.requiredAuth === true) {
      next({ name: "Login" });
    }
    next();
  }

  if (store.state.auth.user !== null && to.path == "/login") {
    if (
      store.state.auth.user.roles.includes("ROLE_EKSPEDISI") &&
      store.state.auth.user.roles.length === 1
    ) {
      next({ name: "ticketList" });
    } else {
      next({ name: "DashboardUtama" });
    }
  }

  /* required auth route route */
  if (authorize) {
    if (
      authorize.length &&
      !authorize.some((x) => store.state.auth.user.roles.includes(x))
    ) {
      console.log(
        authorize.some((x) => store.state.auth.user.roles.includes(x))
      );
      next({ name: "Forbidden" });
    }
  }

  /* fetch notification top disemua page kecuali /notification agar tidak double */
  if (to.path !== "/notification" && to.meta.requiredAuth === true) {
    store.dispatch("notification/loadNotificationLabelWithNotificationItem");
    store.dispatch("ticketing/loadTicketingNotification");
  }

  next();
});
export default router;
