export default function authHeaderMultipart() {
  let user = JSON.parse(localStorage.getItem("token"));
  if (user && user.accessToken) {
    return {
      Authorization: "Bearer " + user.accessToken,
      "Access-Control-Allow-Credentials": "true",
      "Content-Type": "multipart/form-data",
    };
  } else {
    return {};
  }
}
