import axios from "axios";
import authHeader from "./auth-header";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.AUTH_SERVICE_URL;

class AuthService {
  login(user) {
    return axios
      .post(API_URL + "signin", {
        username: user.username,
        password: user.password,
      })
      .then((response) => {
        if (response.data.token.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data.data));
          localStorage.setItem("token", JSON.stringify(response.data.token));
          localStorage.setItem("isBannerDashboard", JSON.stringify(true));

          /**
           * test
           */
          // axios.get("http://localhost:8080/api/test/user", {
          //   headers: {
          //     Authorization: "Bearer " + response.data.token.accessToken,
          //   },
          // });

          // console.log(response.data)
        }
        return { ...response.data.data, ...response.data.token };
      });
  }

  removeLocalStorage() {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    localStorage.removeItem("isBannerDashboard");
  }

  logout() {
    const token = JSON.parse(localStorage.getItem("token"));
    return axios
      .post(API_URL + "logout", {
        token: token.refreshToken,
      })
      .then((response) => {
        if (response.data.message === "Log out successful!") {
          this.removeLocalStorage();
        }
      });
  }

  register(user) {
    return axios.post(API_URL + "signup", {
      username: user.username,
      email: user.email,
      password: user.password,
      fdivisionBean: user.fdivisionBean,
      roles: user.roles,
      organizationLevel: user.organizationLevel,
      expedisi: user.expedisi,
      phone: user.phone,
      contryCode: user.countryCode,
      avatarImage: user.avatarImage,
      birthDate: user.birthDate,
      fullName: user.fullName,
      salesmanOf: user.salesmanOf,
    });
  }

  updateUser(user) {
    return axios.post(API_URL + "updateUser", {
      id: user.id,
      username: user.username,
      email: user.email,
      password: user.password,
      fdivisionBean: user.fdivisionBean,
      roles: user.roles,
      organizationLevel: user.organizationLevel,
      expedisi: user.expedisi,
      phone: user.phone,
      contryCode: user.countryCode,
      avatarImage: user.avatarImage,
      birthDate: user.birthDate,
      fullName: user.fullName,
      salesmanOf: user.salesmanOf,
      isAccountNonLocked: user.isAccountNonLocked,
    });
  }

  /**
   * User Crud
   */
  getAllUser() {
    return axios.get(API_URL + "user", { headers: authHeader() });
  }

  getAllSimpleUser() {
    return axios.get(API_URL + "getAllUserSimple", { headers: authHeader() });
  }
}

export default new AuthService();
