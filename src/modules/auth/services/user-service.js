import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.AUTH_SERVICE_URL;

class UserService {
  getApiUrl() {
    return API_URL;
  }
  getAllUser(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllUserContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getUserById(id) {
    return axios.get(API_URL + `getUserById/${id}`);
  }
  getUserByUsername(username) {
    return axios.get(API_URL + `getUserByUsername/${username}`);
  }

  updateUser(item) {
    return axios.put(API_URL + `updateUser/${item.id}`, item);
  }
  createUser(item) {
    return axios.post(API_URL + `createUser`, item);
  }
  deleteUser(id) {
    return axios.delete(API_URL + `deleteUser/${id}`);
  }
  deleteAllUser(itemIds) {
    return axios.delete(API_URL + `deleteAllUser`, {
      data: itemIds,
    });
  }
}

export default new UserService();
