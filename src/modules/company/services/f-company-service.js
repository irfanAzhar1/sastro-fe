import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FCompanyService {
  getApiUrl() {
    return API_URL;
  }
  getAllFCompany() {
    return axios.get(API_URL + `getAllFCompany`);
  }
  getAllFCompanyContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFCompanyContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFCompanyById(id) {
    return axios.get(API_URL + `getFCompanyById/${id}`);
  }
  updateFCompany(item) {
    return axios.put(API_URL + `updateFCompany/${item.id}`, item);
  }
  createFCompany(item) {
    return axios.post(API_URL + `createFCompany`, item);
  }
  deleteFCompany(id) {
    return axios.delete(API_URL + `deleteFCompany/${id}`);
  }
  deleteAllFCompany(itemIds) {
    return axios.delete(API_URL + `deleteAllFCompany`, {
      data: itemIds,
    });
  }
}
export default new FCompanyService();
