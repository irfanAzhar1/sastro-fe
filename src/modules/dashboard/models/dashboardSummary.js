export default class DashboardSummary {
  constructor(
    fdivisionIds = [],
    fsalesmanIds = [],
    fstoreIds = [],
    fexpedisiIds = [],
    statusPengirimanIds = [],
    fmaterialIds = [],
    fwarehouseIds = [],
    dateFrom = new Date(),
    dateTo = new Date(),
    filterBy = "all"
  ) {
    this.fdivisionIds = fdivisionIds;
    this.fsalesmanIds = fsalesmanIds;
    this.fstoreIds = fstoreIds;
    this.fexpedisiIds = fexpedisiIds;
    this.statusPengirimanIds = statusPengirimanIds;
    this.fmaterialIds = fmaterialIds;
    this.fwarehouseIds = fwarehouseIds;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.filterBy = filterBy;
  }
}
