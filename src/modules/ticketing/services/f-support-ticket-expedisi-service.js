import axios from "@/plugins/axios"
import ConstApiUrls from "../../../services/const-api-urls"

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FSupportTicketExpedisiService {
  getApiUrl(){
    return API_URL;
  }
  createFSupportExpedisiTicket(item) {
    return axios.post(API_URL + `support-ticket/supportTicketEkspedisi`,item)
  }
  salesTransactionExpedisiByDeliveryNumber(deliveryNumber){
    return axios.get(API_URL + `support-ticket/supportTicketEkspedisiDeliveryNumber?deliveryNumber=${deliveryNumber}`)
  }
  getAllSupportTicketContainingExpedisi(page,pageSize,sortBy,order,deliveryNumber,expedisiIds,dateStart,dateEnd,status){
    return axios.get(API_URL + `support-ticket/supportTicketEkspedisiPaginated?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&deliveryNumber=${deliveryNumber}&expedisiIds=${expedisiIds}&dateStart=${dateStart}&dateEnd=${dateEnd}&status=${status}`)
  }
  getSummaryEkspedisiTicketing(
    dateStart,
    dateEnd,
    status,
    deliveryNumber,
    expedisiIds,
    isEkspedisi,
  ) {
    return axios.get(API_URL + `support-ticket/getSummaryTicketing?dateStart=${dateStart}&dateEnd=${dateEnd}&status=${status}&deliveryNumber=${deliveryNumber}&ekspedisiIds=${expedisiIds}&isEkspedisi=${isEkspedisi}`)
  }
  createFSupportExpedisiTicketByImport(item){
    return axios.post(API_URL + `support-ticket/supportTicketEkspedisiImport`,item)
  }
}

export default new FSupportTicketExpedisiService
