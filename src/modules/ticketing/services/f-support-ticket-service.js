import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FSupportTicketService {
  getApiUrl() {
    return API_URL;
  }
  getAllSupportTicketContainingNonAdmin(
    page,
    pageSize,
    sortBy,
    order,
    deliveryNumber,
    categoryId,
    status
  ) {
    return axios.get(
      API_URL +
        `user-support-ticket?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&deliveryNumber=${deliveryNumber}&categoryId=${categoryId}&status=${status}`
    );
  }
  getAllSupportTicketContainingAdmin(
    page,
    pageSize,
    sortBy,
    order,
    deliveryNumber,
    categoryId,
    status,
    expedisiIds,
    dateStart,
    dateEnd
  ) {
    return axios.get(
      API_URL +
        `support-ticket?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&deliveryNumber=${deliveryNumber}&categoryId=${categoryId}&status=${status}&expedisiIds=${expedisiIds}&dateStart=${dateStart}&dateEnd=${dateEnd}`
    );
  }
  openFSupportTicket(id) {
    return axios.get(API_URL + `support-ticket/${id}/open`);
  }
  createFSupportTicket(item) {
    return axios.post(API_URL + `support-ticket`, item);
  }
  updateFSupportTicket(item, id) {
    return axios.put(API_URL + `support-ticket/${id}`, item);
  }
  deleteFSupportTicket(id) {
    return axios.delete(API_URL + `support-ticket/${id}`);
  }
  userSalesTransaction() {
    return axios.get(API_URL + `support-ticket/user-sales-transaction`);
  }
  salesTransactionByDeliveryNumber(deliveryNumber) {
    return axios.get(
      API_URL +
        `support-ticket/sales-transaction?deliveryNumber=${deliveryNumber}`
    );
  }
  getConversationTicketing(id) {
    return axios.get(API_URL + `support-ticket/${id}/conversation`);
  }
  createConversationTicketing(id, item) {
    return axios.post(API_URL + `support-ticket/${id}/conversation`, item);
  }
  readConversationTicketing(id) {
    return axios.get(API_URL + `support-ticket/${id}/readConversation`);
  }
  getSupportTicketConversationPaginated(page, pageSize, sortBy, order) {
    return axios.get(
      API_URL +
        `support-ticket/supportTicketConversationPaginated?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}`
    );
  }
  filterTicketing(payload) {
    return axios.post(API_URL + `user-support-ticket`, payload);
  }
  getSummaryTicketing(
    deliveryNumber,
    expedisiIds,
    status,
    dateEnd,
    dateStart,
    isEkspedisi
  ) {
    return axios.get(
      API_URL +
        `support-ticket/getSummaryTicketing?dateStart=${dateStart}&dateEnd=${dateEnd}&status=${status}&deliveryNumber=${deliveryNumber}&ekspedisiIds=${expedisiIds}&isEkspedisi=${isEkspedisi}`
    );
  }
}

export default new FSupportTicketService();
