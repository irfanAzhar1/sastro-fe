import axios from "axios";
import authHeader from "../../auth/services/auth-header";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FTicketMasterStatus {
  getApiUrl() {
    return API_URL;
  }
  getAllTicketMasterStatus(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `support-ticket/masterStatus?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`,
      { headers: authHeader() }
    );
  }
  getAllMasterStatus() {
    return axios.get(API_URL + `support-ticket/masterStatusAll`, {
      headers: authHeader(),
    });
  }
  createFTicketMasterStatus(payload) {
    return axios.post(API_URL + `support-ticket/masterStatus`, payload, {
      headers: authHeader(),
    });
  }
  updateFTicketMasterStatus(id, payload) {
    return axios.put(
      API_URL + `support-ticket/masterStatus/${id}/update`,
      payload,
      { headers: authHeader() }
    );
  }
  deleteFTicketMasterStatus(id) {
    return axios.delete(API_URL + `support-ticket/masterStatus/${id}/delete`, {
      headers: authHeader(),
    });
  }
  getTicketMasterStatusById(id) {
    return axios.get(API_URL + `support-ticket/masterStatus/${id}`, {
      headers: authHeader(),
    });
  }
}

export default new FTicketMasterStatus();
