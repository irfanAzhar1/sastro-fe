import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FCategoryTicketService {
  getApiUrl() {
    return API_URL;
  }
  getAllTicketCategory(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `support-ticket-category?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getAllCategory() {
    return axios.get(API_URL + `all-support-ticket-category`);
  }
  getTicketCategoryById(id) {
    return axios.get(API_URL + `support-ticket-category/${id}`);
  }
  createFTicketCategory(item) {
    return axios.post(API_URL + `support-ticket-category`, item);
  }
  updateFTicketCategory(id, item) {
    return axios.put(API_URL + `support-ticket-category/${id}`, item);
  }
  deleteFTicketCategory(id) {
    return axios.delete(API_URL + `support-ticket-category/${id}`);
  }
}
export default new FCategoryTicketService();
