import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FCategoryTicketExpedisiService {
  getApiUrl() {
    return API_URL;
  }
  getAllTicketCategoryExpedisi(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `support-ticket/supportTicketCategoryEkspedisi?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getAllCategoryExpedisi() {
    return axios.get(
      API_URL + `support-ticket/supportTicketCategoryEkspedisiAll`
    );
  }
  createFTicketCategoryExpedisi(item) {
    return axios.post(
      API_URL + `support-ticket/supportTicketCategoryEkspedisi`,
      item
    );
  }
  updateFTicketCategoryExpedisi(id, item) {
    return axios.put(
      API_URL + `support-ticket/supportTicketCategoryEkspedisi/${id}/update`,
      item
    );
  }
  deleteFTicketCategoryExpedisi(id) {
    return axios.delete(
      API_URL + `support-ticket/supportTicketCategoryEkspedisi/${id}/delete`
    );
  }
  getTicketCategoryExpedisiById(id) {
    return axios.get(
      API_URL + `support-ticket/supportTicketCategoryEkspedisiById/${id}`
    );
  }
}
export default new FCategoryTicketExpedisiService();
