export default class FSupportTicketExpedisi {
  constructor(
    id,
    support_ticket_category_ekspedisi_id,
    sales_transaction_id,
    description,
    message,
    priority,
    image_file_name
  ) {
    this.id = id;
    this.description = description;
    this.support_ticket_category_ekspedisi_id = support_ticket_category_ekspedisi_id;
    this.sales_transaction_id = sales_transaction_id;
    this.message = message;
    this.priority = priority;
    this.image_file_name = image_file_name;
  }
}
