import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;
const API_URL_V1 = ConstApiUrls.VITE_API_SERVICE_URL_V1;

class FCatalogProductService {
  getApiUrl() {
    return API_URL;
  }
  getAllFAreaContaining(
    page,
    pageSize,
    sortBy,
    order,
    search,
    brand,
    kategori
  ) {
    return axios.get(
      API_URL +
        `public/getPaginatedProductCatalog?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}&brand=${brand}&kategori=${kategori}`
    );
  }
  getAllPaginatedProductCatalog(payload){
    return axios.post(API_URL_V1 + `products/catalogs`,payload);
  }
  getCategoryGroup3(payload){
    return axios.post(API_URL + `categoryGroup3WithCountAndLimit`,payload)
  }
  getBrand(payload){
    return axios.post(API_URL + `getMaterialBrandByDescriptionOrKode1AndLimit`,payload)
  }
}
export default new FCatalogProductService();
