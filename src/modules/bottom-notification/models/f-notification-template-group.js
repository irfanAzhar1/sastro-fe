export default class FNotificationGroupTemplate {
    constructor(
        id,
        template,
        group_id,
    ) {
        this.id = id;
        this.template = template;
        this.group_id = group_id;
    }

}
