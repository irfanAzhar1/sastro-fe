import axios from "@/plugins/axios";
import ConstApiUrls from "../../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FCustomerGroupService {
  getApiUrl() {
    return API_URL;
  }
  getAllFCustomerGroup() {
    return axios.get(API_URL + `getAllFCustomerGroup`);
  }
  getAllFCustomerGroupSimple() {
    return axios.get(API_URL + `getAllFCustomerGroupSimple`);
  }
  getAllFCustomerGroupByCompany() {
    return axios.get(API_URL + `getAllFCustomerGroupByCompany`);
  }
  getAllFCustomerGroupByCompanySimple() {
    return axios.get(API_URL + `getAllFCustomerGroupByCompanySimple`);
  }
  getAllFCustomerGroupContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFCustomerGroupContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFCustomerGroupById(id) {
    return axios.get(API_URL + `getFCustomerGroupById/${id}`);
  }
  updateFCustomerGroup(item) {
    return axios.put(API_URL + `updateFCustomerGroup/${item.id}`, item);
  }
  createFCustomerGroup(item) {
    return axios.post(API_URL + `createFCustomerGroup`, item);
  }
  deleteFCustomerGroup(id) {
    return axios.delete(API_URL + `deleteFCustomerGroup/${id}`);
  }
  deleteAllFCustomerGroup(itemIds) {
    return axios.delete(API_URL + `deleteAllFCustomerGroup`, {
      data: itemIds,
    });
  }
}
export default new FCustomerGroupService();
