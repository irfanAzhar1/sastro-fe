import axios from "@/plugins/axios";
import ConstApiUrls from "../../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FCustomerService {
  getApiUrl() {
    return API_URL;
  }
  getAllFCustomer() {
    return axios.get(API_URL + `getAllFCustomer`);
  }
  getAllFCustomerSimple() {
    return axios.get(API_URL + `getAllFCustomerSimple`);
  }
  getAllSearchFCustomerSimple(search) {
    return axios.get(API_URL + `getAllFCustomerSimpleSearch?query=` + search);
  }
  getAllFCustomerBySalesman(fsalesmanBean) {
    return axios.get(API_URL + `getAllFCustomerBySalesman/${fsalesmanBean}`);
  }
  getAllFCustomerBySalesmanAndCustnameContaining(fsalesmanBean, search) {
    return axios.get(
      API_URL + `getAllFCustomerBySalesman/${fsalesmanBean}/${search}`
    );
  }
  getAllFCustomerContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFCustomerContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFCustomerById(id) {
    return axios.get(API_URL + `getFCustomerById/${id}`);
  }
  getFCustomerByIdSimple(id) {
    return axios.get(API_URL + `getFCustomerByIdSimple/${id}`);
  }
  updateFCustomer(item) {
    return axios.put(API_URL + `updateFCustomer/${item.id}`, item);
  }
  createFCustomer(item) {
    return axios.post(API_URL + `createFCustomer`, item);
  }
  deleteFCustomer(id) {
    return axios.delete(API_URL + `deleteFCustomer/${id}`);
  }
  deleteAllFCustomer(itemIds) {
    return axios.delete(API_URL + `deleteAllFCustomer`, {
      data: itemIds,
    });
  }
}
export default new FCustomerService();
