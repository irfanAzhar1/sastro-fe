import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FVendorService {
  getApiUrl() {
    return API_URL;
  }
  getAllFVendor() {
    return axios.get(API_URL + `getAllFVendor`);
  }
  getAllFVendorContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFVendorContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFVendorById(id) {
    return axios.get(API_URL + `getFVendorById/${id}`);
  }
  updateFVendor(item) {
    return axios.put(API_URL + `updateFVendor/${item.id}`, item);
  }
  createFVendor(item) {
    return axios.post(API_URL + `createFVendor`, item);
  }
  deleteFVendor(id) {
    return axios.delete(API_URL + `deleteFVendor/${id}`);
  }
  deleteAllFVendor(itemIds) {
    return axios.delete(API_URL + `deleteAllFVendor`, {
      data: itemIds,
    });
  }
}
export default new FVendorService();
