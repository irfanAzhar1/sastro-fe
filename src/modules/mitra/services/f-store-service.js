import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FStoreService {
  getApiUrl() {
    return API_URL;
  }
  getAllFStore() {
    return axios.get(API_URL + `getAllFStore`);
  }
  getAllFStoreSimple() {
    return axios.get(API_URL + `getAllFStoreSimple`);
  }
  getAllFStoreContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFStoreContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getAllFStoreContainingAndFsalesmanBean(
    page,
    pageSize,
    sortBy,
    order,
    search,
    fsalesmanBean
  ) {
    return axios.get(
      API_URL +
        `getAllFStoreContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}&fsalesmanBean=${fsalesmanBean}`
    );
  }
  getFStoreById(id) {
    return axios.get(API_URL + `getFStoreById/${id}`);
  }
  getAllFStoreByDescription(description) {
    return axios.get(API_URL + `getAllFStoreByDescription/${description}`);
  }

  updateFStore(item) {
    return axios.put(API_URL + `updateFStore/${item.id}`, item);
  }
  createFStore(item) {
    return axios.post(API_URL + `createFStore`, item);
  }
  deleteFStore(id) {
    return axios.delete(API_URL + `deleteFStore/${id}`);
  }
  deleteAllFStore(itemIds) {
    return axios.delete(API_URL + `deleteAllFStore`, {
      data: itemIds,
    });
  }
}
export default new FStoreService();
