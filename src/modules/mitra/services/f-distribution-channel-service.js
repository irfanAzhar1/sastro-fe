import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FDistributionChannelService {
  getApiUrl() {
    return API_URL;
  }
  getAllFDistributionChannel() {
    return axios.get(API_URL + `getAllFDistributionChannel`);
  }
  getAllFDistributionChannelSimple() {
    return axios.get(API_URL + `getAllFDistributionChannelSimple`);
  }
  getAllFDistributionChannelByCompany() {
    return axios.get(API_URL + `getAllFDistributionChannelByCompany`);
  }
  getAllFDistributionChannelByCompanySimple() {
    return axios.get(API_URL + `getAllFDistributionChannelByCompanySimple`);
  }
  getAllFDistributionChannelContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFDistributionChannelContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFDistributionChannelById(id) {
    return axios.get(API_URL + `getFDistributionChannelById/${id}`);
  }
  updateFDistributionChannel(item) {
    return axios.put(API_URL + `updateFDistributionChannel/${item.id}`, item);
  }
  createFDistributionChannel(item) {
    return axios.post(API_URL + `createFDistributionChannel`, item);
  }
  deleteFDistributionChannel(id) {
    return axios.delete(API_URL + `deleteFDistributionChannel/${id}`);
  }
  deleteAllFDistributionChannel(itemIds) {
    return axios.delete(API_URL + `deleteAllFDistributionChannel`, {
      data: itemIds,
    });
  }
}
export default new FDistributionChannelService();
