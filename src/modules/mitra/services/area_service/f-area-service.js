import axios from "@/plugins/axios";
import authHeader from "../../../auth/services/auth-header";
import ConstApiUrls from "../../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FAreaService {
  getApiUrl() {
    return API_URL;
  }
  getAllFArea() {
    return axios.get(API_URL + `getAllFArea`);
  }
  getAllFAreaSimple() {
    return axios.get(API_URL + `getAllFAreaSimple`);
  }
  getAllFAreaByCompany() {
    return axios.get(API_URL + `getAllFAreaByCompany`, {
      headers: authHeader(),
    });
  }
  getAllFAreaContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFAreaContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFAreaById(id) {
    return axios.get(API_URL + `getFAreaById/${id}`);
  }
  updateFArea(item) {
    return axios.put(API_URL + `updateFArea/${item.id}`, item);
  }
  createFArea(item) {
    return axios.post(API_URL + `createFArea`, item);
  }
  deleteFArea(id) {
    return axios.delete(API_URL + `deleteFArea/${id}`);
  }
  deleteAllFArea(itemIds) {
    return axios.delete(API_URL + `deleteAllFArea`, {
      data: itemIds,
    });
  }
}
export default new FAreaService();
