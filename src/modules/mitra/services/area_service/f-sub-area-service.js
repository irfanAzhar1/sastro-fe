import axios from "@/plugins/axios";
import ConstApiUrls from "../../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FSubAreaService {
  getApiUrl() {
    return API_URL;
  }
  getAllFSubArea() {
    return axios.get(API_URL + `getAllFSubArea`);
  }
  getAllFSubAreaSimple() {
    return axios.get(API_URL + `getAllFSubAreaSimple`);
  }
  getAllFSubAreaByCompany() {
    return axios.get(API_URL + `getAllFSubAreaByCompany`);
  }
  getAllFSubAreaContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFSubAreaContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getAllFSubAreaSimpleBySearch(search) {
    return axios.get(API_URL + `getAllFSubAreaSimpleBySearch?search=${search}`);
  }
  findFAreaFRegionByFSubArea(id) {
    return axios.get(API_URL + `findFAreaFRegionByFSubArea/${id}`);
  }
  getFSubAreaById(id) {
    return axios.get(API_URL + `getFSubAreaById/${id}`);
  }
  getFSubAreaDetailAliasById(id) {
    return axios.get(API_URL + `getFSubAreaDetailAliasById/${id}`);
  }
  updateFSubArea(item) {
    return axios.put(API_URL + `updateFSubArea/${item.id}`, item);
  }
  createFSubArea(item) {
    return axios.post(API_URL + `createFSubArea`, item);
  }
  deleteFSubArea(id) {
    return axios.delete(API_URL + `deleteFSubArea/${id}`);
  }
  deleteAllFSubArea(itemIds) {
    return axios.delete(API_URL + `deleteAllFSubArea`, {
      data: itemIds,
    });
  }
}
export default new FSubAreaService();
