import axios from "@/plugins/axios";
import ConstApiUrls from "../../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FRegionService {
  getApiUrl() {
    return API_URL;
  }
  getAllFRegion() {
    return axios.get(API_URL + `getAllFRegion`);
  }
  getAllFRegionByCompany() {
    return axios.get(API_URL + `getAllFRegionByCompany`);
  }
  getAllFRegionContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFRegionContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFRegionById(id) {
    return axios.get(API_URL + `getFRegionById/${id}`);
  }
  updateFRegion(item) {
    return axios.put(API_URL + `updateFRegion/${item.id}`, item);
  }
  createFRegion(item) {
    return axios.post(API_URL + `createFRegion`, item);
  }
  deleteFRegion(id) {
    return axios.delete(API_URL + `deleteFRegion/${id}`);
  }
  deleteAllFRegion(itemIds) {
    return axios.delete(API_URL + `deleteAllFRegion`, {
      data: itemIds,
    });
  }
}
export default new FRegionService();
