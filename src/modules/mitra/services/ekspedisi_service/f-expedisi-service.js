import axios from "@/plugins/axios";
import ConstApiUrls from "@/services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FExpedisiService {
  getApiUrl() {
    return API_URL;
  }
  getAllFExpedisi() {
    return axios.get(API_URL + `getAllFExpedisi`);
  }
  getAllFExpedisiByCompany() {
    return axios.get(API_URL + `getAllFExpedisiByCompany`);
  }
  getAllFExpedisiContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFExpedisiContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFExpedisiById(id) {
    return axios.get(API_URL + `getFExpedisiById/${id}`);
  }
  updateFExpedisi(item) {
    return axios.put(API_URL + `updateFExpedisi/${item.id}`, item);
  }
  createFExpedisi(item) {
    return axios.post(API_URL + `createFExpedisi`, item);
  }
  deleteFExpedisi(id) {
    return axios.delete(API_URL + `deleteFExpedisi/${id}`);
  }
  deleteAllFExpedisi(itemIds) {
    return axios.delete(API_URL + `deleteAllFExpedisi`, {
      data: itemIds,
    });
  }
  printResiExpedisi() {
    return axios.post(API_URL + `/report/jasper`);
  }

  getFExpedisiByWarehouseId(warehouse_id) {
    return axios.get(API_URL + `getFExpedisiByWarehouseId/${warehouse_id}`);
  }
}
export default new FExpedisiService();
