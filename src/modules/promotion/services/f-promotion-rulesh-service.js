import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FPromotionRuleshService {
  getApiUrl() {
    return API_URL;
  }
  getAllFPromotionRulesh() {
    return axios.get(API_URL + `getAllFPromotionRulesh`);
  }
  getAllFPromotionRuleshByCompany() {
    return axios.get(API_URL + `getAllFPromotionRuleshByCompany`);
  }
  getAllFPromotionRuleshContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFPromotionRuleshContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFPromotionRuleshById(id) {
    return axios.get(API_URL + `getFPromotionRuleshById/${id}`);
  }
  updateFPromotionRulesh(item) {
    return axios.put(API_URL + `updateFPromotionRulesh/${item.id}`, item);
  }
  createFPromotionRulesh(item) {
    return axios.post(API_URL + `createFPromotionRulesh`, item);
  }
  deleteFPromotionRulesh(id) {
    return axios.delete(API_URL + `deleteFPromotionRulesh/${id}`);
  }
  deleteAllFPromotionRulesh(itemIds) {
    return axios.delete(API_URL + `deleteAllFPromotionRulesh`, {
      data: itemIds,
    });
  }
}
export default new FPromotionRuleshService();
