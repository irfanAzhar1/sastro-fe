import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FPromotionRulesdValidCustsService {
  getApiUrl() {
    return API_URL;
  }
  getAllFPromotionRulesdValidCusts() {
    return axios.get(API_URL + `getAllFPromotionRulesdValidCusts`);
  }
  getAllFPromotionRulesdValidCustsContaining(
    page,
    pageSize,
    sortBy,
    order,
    search
  ) {
    return axios.get(
      API_URL +
        `getAllFPromotionRulesdValidCustsContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFPromotionRulesdValidCustsById(id) {
    return axios.get(API_URL + `getFPromotionRulesdValidCustsById/${id}`);
  }
  getAllFPromotionRulesdValidCustsByParent(id) {
    return axios.get(
      API_URL + `getAllFPromotionRulesdValidCustsByParent/${id}`
    );
  }
  getAllFPromotionRulesdValidCustsByParentIds(ids) {
    return axios.get(
      API_URL + `getAllFPromotionRulesdValidCustsByParentIds/${ids}`
    );
  }
  updateFPromotionRulesdValidCusts(item) {
    return axios.put(
      API_URL + `updateFPromotionRulesdValidCusts/${item.id}`,
      item
    );
  }
  createFPromotionRulesdValidCusts(item) {
    return axios.post(API_URL + `createFPromotionRulesdValidCusts`, item);
  }
  deleteFPromotionRulesdValidCusts(id) {
    return axios.delete(API_URL + `deleteFPromotionRulesdValidCusts/${id}`);
  }
  deleteFPromotionRulesdValidCustsAvatarByParent(fkegiatanBean) {
    return axios.delete(
      API_URL +
        `deleteFPromotionRulesdValidCustsAvatarByParent/${fkegiatanBean}`
    );
  }
  createFPromotionRulesdValidCustsAvatar(item) {
    return axios.post(API_URL + `createFPromotionRulesdValidCustsAvatar`, item);
  }
  deleteAllFPromotionRulesdValidCusts(itemIds) {
    return axios.delete(API_URL + `deleteAllFPromotionRulesdValidCusts`, {
      data: itemIds,
    });
  }
}
export default new FPromotionRulesdValidCustsService();
