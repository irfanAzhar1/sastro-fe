import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FPromotionRulesdValidProductsService {
  getApiUrl() {
    return API_URL;
  }
  getAllFPromotionRulesdValidProducts() {
    return axios.get(API_URL + `getAllFPromotionRulesdValidProducts`);
  }
  getAllFPromotionRulesdValidProductsContaining(
    page,
    pageSize,
    sortBy,
    order,
    search
  ) {
    return axios.get(
      API_URL +
        `getAllFPromotionRulesdValidProductsContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFPromotionRulesdValidProductsById(id) {
    return axios.get(API_URL + `getFPromotionRulesdValidProductsById/${id}`);
  }
  getAllFPromotionRulesdValidProductsByParent(id) {
    return axios.get(
      API_URL + `getAllFPromotionRulesdValidProductsByParent/${id}`
    );
  }
  getAllFPromotionRulesdValidProductsByParentIds(ids) {
    return axios.get(
      API_URL + `getAllFPromotionRulesdValidProductsByParentIds/${ids}`
    );
  }
  updateFPromotionRulesdValidProducts(item) {
    return axios.put(
      API_URL + `updateFPromotionRulesdValidProducts/${item.id}`,
      item
    );
  }
  createFPromotionRulesdValidProducts(item) {
    return axios.post(API_URL + `createFPromotionRulesdValidProducts`, item);
  }
  deleteFPromotionRulesdValidProducts(id) {
    return axios.delete(API_URL + `deleteFPromotionRulesdValidProducts/${id}`);
  }
  deleteFPromotionRulesdValidProductsAvatarByParent(fkegiatanBean) {
    return axios.delete(
      API_URL +
        `deleteFPromotionRulesdValidProductsAvatarByParent/${fkegiatanBean}`
    );
  }
  createFPromotionRulesdValidProductsAvatar(item) {
    return axios.post(
      API_URL + `createFPromotionRulesdValidProductsAvatar`,
      item
    );
  }
  deleteAllFPromotionRulesdValidProducts(itemIds) {
    return axios.delete(API_URL + `deleteAllFPromotionRulesdValidProducts`, {
      data: itemIds,
    });
  }
}
export default new FPromotionRulesdValidProductsService();
