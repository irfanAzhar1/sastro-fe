import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FtSaleshExtService {
  getApiUrl() {
    return API_URL;
  }
  getAllFtSaleshExtByDivision(item) {
    return axios.post(API_URL + `getAllFtSaleshExtByDivision`, item);
  }
  getExportBySalesCs(item) {
    return axios.post(API_URL + `exportSalesCs`, item, {
      responseType: "blob",
    });
  }
  getMultiExport(type, ids) {
    return axios.post(API_URL + `exportSaleMulti/${type}`, ids, {
      responseType: "blob",
    });
  }
  getExportBySalesDetailSimple(item) {
    return axios.post(API_URL + `exportSalesDetailSimple`, item, {
      responseType: "blob",
    });
  }
  getExportBySalesDetail(item) {
    return axios.post(API_URL + `exportSalesDetail`, item, {
      responseType: "blob",
    });
  }
  getExportBySalesDetailDesGreen(item) {
    return axios.post(API_URL + `exportSalesDetailDesGreen`, item, {
      responseType: "blob",
    });
  }
  getAllFtSaleshExtByChannel(item) {
    return axios.post(API_URL + `getAllFtSaleshExtByChannel`, item);
  }
  getAllFtSaleshExtBySalesman(item) {
    return axios.post(API_URL + `getAllFtSaleshExtBySalesman`, item);
  }
  getSummaryDashboard(item) {
    return axios.post(API_URL + `getSummaryDashboard`, item);
  }
  getAllFtSaleshReturnReturnLockReturnComplateExtByExpedisi(item) {
    return axios.post(
      API_URL + `getAllFtSaleshReturnReturnLockReturnComplateExtByExpedisi`,
      item
    );
  }
  getAllFtSaleshSalesByExpedisi(item) {
    return axios.post(API_URL + `getAllFtSaleshExtByExpedisi`, item);
  }
}
export default new FtSaleshExtService();
