import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FScanResiService {
  scanResi(awb, type) {
    return axios.get(API_URL + `v1/scan/${awb}/${type}`);
  }
  postScanResi(item) {
    return axios.post(API_URL + `v1/scan`, item);
  }
  updateResi(item) {
    return axios.post(API_URL + `v1/scan/change_status`, item);
  }
  summaryScan() {
    return axios.get(API_URL + `v1/summaryScanResi`);
  }
  summaryScanDetail(type) {
    return axios.get(API_URL + `v1/summaryScanResi/${type}`);
  }
}

export default new FScanResiService();
