import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FtSalesdItemsService {
  getApiUrl() {
    return API_URL;
  }
  getAllFtSalesdItems() {
    return axios.get(API_URL + `getAllFtSalesdItems`);
  }
  getAllFtSalesdItemsContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFtSalesdItemsContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFtSalesdItemsById(id) {
    return axios.get(API_URL + `getFtSalesdItemsById/${id}`);
  }
  getAllFtSalesdItemsByParent(id) {
    return axios.get(API_URL + `getAllFtSalesdItemsByParent/${id}`);
  }
  getAllFtSalesdItemsByParentIds(ids) {
    return axios.get(API_URL + `getAllFtSalesdItemsByParentIds/${ids}`);
  }
  updateFtSalesdItems(item) {
    return axios.put(API_URL + `updateFtSalesdItems/${item.id}`, item);
  }
  updateFtSalesdItemsCancelQtyByParent(ftSaleshBean) {
    return axios.put(
      API_URL + `updateFtSalesdItemsCancelQtyByParent/${ftSaleshBean.id}`,
      ftSaleshBean
    );
  }
  createFtSalesdItems(item) {
    return axios.post(API_URL + `createFtSalesdItems`, item);
  }
  deleteFtSalesdItems(id) {
    return axios.delete(API_URL + `deleteFtSalesdItems/${id}`);
  }
  deleteFtSalesdItemsIn(ids) {
    return axios.delete(API_URL + `deleteFtSalesdItemsIn`, {
      data: ids,
    });
  }
  deleteFtSalesdItemsAvatarByParent(fkegiatanBean) {
    return axios.delete(
      API_URL + `deleteFtSalesdItemsAvatarByParent/${fkegiatanBean}`
    );
  }
  createFtSalesdItemsAvatar(item) {
    return axios.post(API_URL + `createFtSalesdItemsAvatar`, item);
  }
  deleteAllFtSalesdItems(itemIds) {
    return axios.delete(API_URL + `deleteAllFtSalesdItems`, {
      data: itemIds,
    });
  }
}
export default new FtSalesdItemsService();
