import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FtSaleshService {
  getApiUrl() {
    return API_URL;
  }
  getAllFtSalesh() {
    return axios.get(API_URL + `getAllFtSalesh`);
  }
  getAllFtSaleshContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFtSaleshContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getAllFtSaleshContainingLight(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFtSaleshContainingLight?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getAllFtSaleshByIds(itemIds) {
    return axios.post(API_URL + `getAllFtSaleshByIds`, itemIds);
  }
  getAllFtSalesDetailByIds(itemIds) {
    return axios.post(API_URL + `getAllFtSalesDetailByIds`, itemIds);
  }
  getAllFtSaleshContainingExt(item) {
    // console.log(JSON.stringify(item))
    return axios.post(API_URL + `getAllFtSaleshContainingExt`, item);
  }
  getSalesToItems(item) {
    // console.log(JSON.stringify(item))
    return axios.post(API_URL + `getSalesToItemBySalesman`, item);
  }
  getAllFtSaleshContainingExtLight(item) {
    return axios.post(API_URL + `getAllFtSaleshContainingExtLight`, item);
  }
  getAllFtSaleshContainingAndRemarkInExt(item) {
    // console.log(`${JSON.stringify(item)}`)
    return axios.post(API_URL + `getAllFtSaleshContainingAndRemarkInExt`, item);
  }
  getAllFtSaleshContainingExtSummary(item) {
    return axios.post(API_URL + `getAllFtSaleshContainingExtSummary`, item);
  }
  getAllFtSaleshSummary(item) {
    return axios.post(API_URL + `getAllFtSaleshSummary`, item);
  }
  postReportJasperPrintPos(item, withItem) {
    return axios.post(
      API_URL + `reports/jasper/print_pos?withItem=${withItem}`,
      item,
      { responseType: "blob" }
    );
  }
  postReportJasperResiJnt(item, withItem) {
    return axios.post(
      API_URL + `reports/jasper/resi_jnt?withItem=${withItem}`,
      item,
      { responseType: "blob" }
    );
  }
  postReportJasperResiJne(item, withItem) {
    return axios.post(
      API_URL + `reports/jasper/resi_jne?withItem=${withItem}`,
      item,
      { responseType: "blob" }
    );
  }
  postReportJasperResiIdExpress(item, withItem) {
    return axios.post(
      API_URL + `reports/jasper/resi_id_express?withItem=${withItem}`,
      item,
      { responseType: "blob" }
    );
  }
  postReportJasperResiSiCepat(item, withItem) {
    return axios.post(
      API_URL + `reports/jasper/resi_si_cepat?withItem=${withItem}`,
      item,
      { responseType: "blob" }
    );
  }

  postReportJasperResiSAP(item, withItem) {
    return axios.post(
      API_URL + `reports/jasper/resi_sap?withItem=${withItem}`,
      item,
      { responseType: "blob" }
    );
  }

  postReportJasperResiAnterAja(item, withItem) {
    return axios.post(
      API_URL + `reports/jasper/resi_anter_aja?withItem=${withItem}`,
      item,
      { responseType: "blob" }
    );
  }

  batchUpdateStatusPengiriman(item) {
    return axios.post(API_URL + `batchUpdateStatusPengiriman`, item);
  }
  getFtSaleshById(id) {
    return axios.get(API_URL + `getFtSaleshById/${id}`);
  }
  updateFtSalesh(item) {
    return axios.put(API_URL + `updateFtSalesh/${item.id}`, item);
  }
  updateFtSaleshCanceled(id) {
    return axios.put(API_URL + `updateFtSaleshCanceled/${id}`);
  }
  updateStatusFtSalesh(item) {
    return axios.put(API_URL + `updateFtSaleshStatus`, item);
  }
  updateFtSaleshDeliveryStatusWithTime(item) {
    return axios.put(
      API_URL + `updateFtSaleshDeliveryStatusWithTime/${item.id}`,
      item
    );
  }
  updateFtSaleshCreateInvoiceNo(item) {
    return axios.put(API_URL + `updateFtSaleshCreateInvoiceNo/${item}`);
  }
  updateFtSaleshProcess(item, fwarehouseBean, fexpedisiBean) {
    return axios.put(
      API_URL +
        `updateFtSaleshProcess/${item.id}/${fwarehouseBean}/${fexpedisiBean}`,
      item
    );
  }
  updateFtSaleshCancelAwb(item, fwarehouseBean, fexpedisiBean) {
    return axios.put(
      API_URL +
        `updateFtSaleshCancelAwb/${item.id}/${fwarehouseBean}/${fexpedisiBean}`,
      item
    );
  }
  createFtSalesh(item) {
    return axios.post(API_URL + `createFtSalesh`, item);
  }
  deleteFtSalesh(id) {
    return axios.delete(API_URL + `deleteFtSalesh/${id}`);
  }
  deleteFtSaleshWithChildren(id) {
    return axios.delete(API_URL + `deleteFtSaleshWithChildren/${id}`);
  }
  deleteAllFtSalesh(itemIds) {
    return axios.delete(API_URL + `deleteAllFtSalesh`, {
      data: itemIds,
    });
  }

  createOrder(item) {
    return axios.post(API_URL + `orders`, item);
  }

  orderCalculate(id) {
    return axios.get(API_URL + `orders/${id}/calculate`);
  }

  updateOrder(item, id) {
    return axios.put(API_URL + `orders/${id}`, item);
  }
  processOrder(item, id) {
    return axios.post(API_URL + `orders/${id}/process`, item);
  }

  getOrder(id) {
    return axios.get(API_URL + `orders/${id}/getOrder`);
  }

  cancelOrder(id) {
    return axios.post(API_URL + `orders/${id}/cancel`);
  }

  deleteOrder(id) {
    return axios.delete(API_URL + `orders/${id}/delete`);
  }
}
export default new FtSaleshService();
