import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FPegawaiService {
  getApiUrl() {
    return API_URL;
  }
  getAllFSalesman() {
    return axios.get(API_URL + `getAllFSalesman`);
  }
  getAllFPegawai() {
    return axios.get(API_URL + `getAllFSalesman`);
  }
  getAllFSalesmanContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFSalesmanContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getAllFPegawaiContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFSalesmanContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getAllFSalesmanByDivision(fdivisionBean) {
    return axios.get(API_URL + `getAllFSalesmanByDivision/${fdivisionBean}`);
  }
  getFSalesmanById(id) {
    return axios.get(API_URL + `getFSalesmanById/${id}`);
  }
  getFPegawaiById(id) {
    return axios.get(API_URL + `getFSalesmanById/${id}`);
  }
  updateFSalesman(item) {
    return axios.put(API_URL + `updateFSalesman/${item.id}`, item);
  }
  updateFPegawai(item) {
    return axios.put(API_URL + `updateFSalesman/${item.id}`, item);
  }
  createFSalesman(item) {
    return axios.post(API_URL + `createFSalesman`, item);
  }
  createFPegawai(item) {
    return axios.post(API_URL + `createFSalesman`, item);
  }
  deleteFSalesman(id) {
    return axios.delete(API_URL + `deleteFSalesman/${id}`);
  }
  deleteAllFSalesman(itemIds) {
    return axios.delete(API_URL + `deleteAllFSalesman`, {
      data: itemIds,
    });
  }
}
export default new FPegawaiService();
