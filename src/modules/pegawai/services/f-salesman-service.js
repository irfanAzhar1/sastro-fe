import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FSalesmanService {
  getApiUrl() {
    return API_URL;
  }
  getAllFSalesman() {
    return axios.get(API_URL + `getAllFSalesman`);
  }
  getAllFSalesmanSimple() {
    return axios.get(API_URL + `getAllFSalesmanSimple`);
  }
  getAllFSalesmanByCompany() {
    return axios.get(API_URL + `getAllFSalesman`);
  }
  getAllMyAgenChildrenAndMe() {
    return axios.get(API_URL + `getAllMyAgenChildrenAndMe`);
  }
  getAllFSalesmanContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFSalesmanContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFSalesmanById(id) {
    return axios.get(API_URL + `getFSalesmanById/${id}`);
  }
  updateFSalesman(item) {
    return axios.put(API_URL + `updateFSalesman/${item.id}`, item);
  }
  updateFSalesmanProfile(item) {
    return axios.put(API_URL + `updateFSalesmanProfile/${item.id}`, item);
  }
  createFSalesman(item) {
    return axios.post(API_URL + `createFSalesman`, item);
  }
  deleteFSalesman(id) {
    return axios.delete(API_URL + `deleteFSalesman/${id}`);
  }
  deleteAllFSalesman(itemIds) {
    return axios.delete(API_URL + `deleteAllFSalesman`, {
      data: itemIds,
    });
  }
}
export default new FSalesmanService();
