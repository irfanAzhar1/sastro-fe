import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FDivisonService {
  getApiUrl() {
    return API_URL;
  }
  getAllFDivision() {
    return axios.get(API_URL + `getAllFDivision`);
  }
  getAllFDivisionByOrgLevel() {
    return axios.get(API_URL + `getAllFDivisionByOrgLevel`);
  }
  getAllFDivisionContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFDivisionContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFDivisionById(id) {
    return axios.get(API_URL + `getFDivisionById/${id}`);
  }
  updateFDivision(item) {
    return axios.put(API_URL + `updateFDivision/${item.id}`, item);
  }
  createFDivision(item) {
    return axios.post(API_URL + `createFDivision`, item);
  }
  deleteFDivision(id) {
    return axios.delete(API_URL + `deleteFDivision/${id}`);
  }
  deleteAllFDivision(itemIds) {
    return axios.delete(API_URL + `deleteAllFDivision`, {
      data: itemIds,
    });
  }
}
export default new FDivisonService();
