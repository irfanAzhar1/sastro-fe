import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FtArPaymentdService {
  getApiUrl() {
    return API_URL;
  }
  getAllFtArPaymentd() {
    return axios.get(API_URL + `getAllFtArPaymentd`);
  }
  getAllFtArPaymentdContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFtArPaymentdContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFtArPaymentdById(id) {
    return axios.get(API_URL + `getFtArPaymentdById/${id}`);
  }
  getAllFtArPaymentdByParent(id) {
    return axios.get(API_URL + `getAllFtArPaymentdByParent/${id}`);
  }
  getAllFtArPaymentdByParentIds(ids) {
    return axios.get(API_URL + `getAllFtArPaymentdByParentIds/${ids}`);
  }

  getAllFtArPaymentdByParentInvoice(id) {
    return axios.get(API_URL + `getAllFtArPaymentdByParentInvoice/${id}`);
  }
  getAllFtArPaymentdByParentPayment(id) {
    return axios.get(API_URL + `getAllFtArPaymentdByParentPayment/${id}`);
  }
  updateFtArPaymentd(item) {
    return axios.put(API_URL + `updateFtArPaymentd/${item.id}`, item);
  }
  createFtArPaymentd(item) {
    return axios.post(API_URL + `createFtArPaymentd`, item);
  }
  deleteFtArPaymentd(id) {
    return axios.delete(API_URL + `deleteFtArPaymentd/${id}`);
  }
  deleteFtArPaymentdAvatarByParent(fkegiatanBean) {
    return axios.delete(
      API_URL + `deleteFtArPaymentdAvatarByParent/${fkegiatanBean}`
    );
  }
  createFtArPaymentdAvatar(item) {
    return axios.post(API_URL + `createFtArPaymentdAvatar`, item);
  }
  deleteAllFtArPaymentd(itemIds) {
    return axios.delete(API_URL + `deleteAllFtArPaymentd`, {
      data: itemIds,
    });
  }
}
export default new FtArPaymentdService();
