import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FBannerService {
  getApiUrl() {
    return API_URL;
  }
  // getAllFArea(){
  //   return axios.get(API_URL + `getAllFArea`);
  // }
  // getAllFAreaSimple(){
  //   return axios.get(API_URL + `getAllFAreaSimple`);
  // }
  // getAllFAreaByCompany(){
  //   return axios.get(API_URL + `getAllFAreaByCompany`);
  // }
  getNotificationBannerContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getNotificationBannerContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getNotificationBannerContainingDashboard(page, pageSize, sortBy, order) {
    return axios.get(
      API_URL +
        `getFBannerNotification?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}`
    );
  }
  getNotificationBannerById(id) {
    return axios.get(API_URL + `getNotificationBannerById/${id}`);
  }
  updateFBanner(item) {
    return axios.put(API_URL + `updateFBanner/${item.id}`, item);
  }
  createFBanner(item) {
    return axios.post(API_URL + `createFBanner`, item);
  }
  deleteFBannerById(id) {
    return axios.delete(API_URL + `deleteNotificationBanner/${id}`);
  }
  // deleteAllFArea(itemIds){
  //   return axios.delete(API_URL + `deleteAllFArea`, {
  //     headers:  authHeader(),
  //     data: itemIds
  //   });
  // }
}
export default new FBannerService();
