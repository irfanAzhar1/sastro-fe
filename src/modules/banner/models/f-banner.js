export default class FBanner {
  constructor(
    id,
    imagePath,
    isRedirect = false,
    priority,
    titleNotification,
    urlRedirect
  ) {
    this.id = id;
    this.imagePath = imagePath;
    this.isRedirect = isRedirect;
    this.priority = priority;
    this.titleNotification = titleNotification;
    this.urlRedirect = urlRedirect;
  }

}
