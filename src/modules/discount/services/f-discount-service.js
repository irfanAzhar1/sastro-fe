import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.VITE_API_SERVICE_URL_V1;

class FDiscountService {
  getApiUrl() {
    return API_URL;
  }
  getAllPaginated(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `discounts/paginated?pageNo=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getAllActive() {
    return axios.get(API_URL + `discounts/active`);
  }
  update(id, item) {
    return axios.put(API_URL + `discounts/${id}`, item);
  }
  create(item) {
    return axios.post(API_URL + `discounts`, item);
  }
  delete(id) {
    return axios.delete(API_URL + `discounts/${id}`);
  }
}
export default new FDiscountService();
