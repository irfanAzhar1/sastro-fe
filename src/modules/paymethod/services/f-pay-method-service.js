import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FPayMethodService {
  getApiUrl() {
    return API_URL;
  }
  getAllFPayMethod() {
    return axios.get(API_URL + `getAllFPayMethod`);
  }
  getAllFPayMethodByCompany() {
    return axios.get(API_URL + `getAllFPayMethodByCompany`);
  }
  getAllFPayMethodContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFPayMethodContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFPayMethodById(id) {
    return axios.get(API_URL + `getFPayMethodById/${id}`);
  }
  updateFPayMethod(item) {
    return axios.put(API_URL + `updateFPayMethod/${item.id}`, item);
  }
  createFPayMethod(item) {
    return axios.post(API_URL + `createFPayMethod`, item);
  }
  deleteFPayMethod(id) {
    return axios.delete(API_URL + `deleteFPayMethod/${id}`);
  }
  deleteAllFPayMethod(itemIds) {
    return axios.delete(API_URL + `deleteAllFPayMethod`, {
      data: itemIds,
    });
  }
}
export default new FPayMethodService();
