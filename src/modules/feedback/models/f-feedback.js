export default class FFeedback {
  constructor(
    id,
    title = "",
    description = "",
    feedbackCategoryBean = 0,
    userBean = "",
    imagePath = "",
    created,
    modified
  ) {
    this.id = id;
    this.title = title;
    this.imagePath = imagePath;
    this.description = description;
    this.feedbackCategoryBean = feedbackCategoryBean;
    this.userBean = userBean;
    this.created = created;
    this.modified = modified;
  }
}
