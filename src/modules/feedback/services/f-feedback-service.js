import axios from "@/plugins/axios";
import ConstApiUrls from "@/services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FFeedbackService {
  getApiUrl() {
    return API_URL;
  }
  getAllFFeedbackService() {
    return axios.get(API_URL + `getAllFeedback`);
  }
  getAllFFeedbackServiceContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFeedbackContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFFeedbackServiceById(id) {
    return axios.get(API_URL + `getFeedbackById/${id}`);
  }
  updateFFeedbackService(item) {
    return axios.put(API_URL + `updateFeedback/${item.id}`, item);
  }
  createFFeedbackService(item) {
    return axios.post(API_URL + `createFeedback`, item);
  }
  deleteFFeedbackService(id) {
    return axios.delete(API_URL + `deleteFeedback/${id}`);
  }
}
export default new FFeedbackService();
