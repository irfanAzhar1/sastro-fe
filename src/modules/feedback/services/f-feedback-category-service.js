import axios from "@/plugins/axios";
import ConstApiUrls from "@/services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FFeedbackCategoryService {
  getApiUrl() {
    return API_URL;
  }
  getAllFFeedbackCategoryService() {
    return axios.get(API_URL + `getAllFeedbackCategory`);
  }
  getAllFFeedbackCategoryServiceContaining(
    page,
    pageSize,
    sortBy,
    order,
    search
  ) {
    return axios.get(
      API_URL +
        `getAllFeedbackCategoryContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFFeedbackCategoryServiceById(id) {
    return axios.get(API_URL + `getFeedbackCategoryById/${id}`);
  }
  updateFFeedbackCategoryService(item) {
    return axios.put(API_URL + `updateFeedbackCategory/${item.id}`, item);
  }
  createFFeedbackCategoryService(item) {
    return axios.post(API_URL + `createFeedbackCategory`, item);
  }
  deleteFFeedbackCategoryService(id) {
    return axios.delete(API_URL + `deleteFeedbackCategory/${id}`);
  }
}
export default new FFeedbackCategoryService();
