import axios from "@/plugins/axios";
import ConstApiUrls from "@/services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FGeneralConfigService {
  getApiUrl() {
    return API_URL;
  }
  getAllFGeneralConfig() {
    return axios.get(API_URL + `getAllFGeneralConfig`);
  }
  getAllFGeneralConfigContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllGeneralConfigContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFGeneralConfigById(id) {
    return axios.get(API_URL + `getFGeneralConfigById/${id}`);
  }
  updateFGeneralConfig(item) {
    return axios.put(API_URL + `UpdateGeneralConfig/${item.id}`, item);
  }
}
export default new FGeneralConfigService();
