export default class FGeneralConfig {
   constructor(
      id,
      namaConfig,
      value,
      created,
      modified,
      modifiedBy
   ) {
     this.id = id;
     this.namaConfig = namaConfig;
     this.value = value;
     this.created = created;
     this.modified = modified;
     this.modifiedBy = modifiedBy;
   }
 }
 