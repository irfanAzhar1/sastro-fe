export default class FMaterial {
  constructor(
    pcode = "",
    pname = "",
    avatarImage = "",
    description = "",
    statusActive,
    showCatalog,
    taxable,

    uom1 = "",
    uom2 = "",
    uom3 = "",
    uom4 = "",
    convfact1 = 1,
    convfact2 = 1,
    convfact3 = 1,

    basePrice = 0,
    hpp = 0,

    basePriceWithTax = 0,
    hppWithTax = 0,

    hpp_distributor = 0,
    hpp_agen = 0,
    hpp_resseler_1 = 0,
    hpp_resseler_2 = 0,

    weightSmalest = 0,
    caseWeight = 0,

    /* dimatikan sementara, sepertinya ini tidak dipakai */
    // materialType = "FERT",

    fdivisionBean,
    fmaterialGroup3Bean,
    fmaterialSalesBrandBean,
    ftaxBean = null,
    fvendorBean,

    distributionChannels = [],
    fwarehouses = [],
    materialMaps = [],
    prices = []
  ) {
    this.pcode = pcode;
    this.pname = pname;
    this.avatarImage = avatarImage;
    this.description = description;
    this.statusActive = statusActive;
    this.showCatalog = showCatalog;
    this.taxable = taxable;

    this.uom1 = uom1;
    this.uom2 = uom2;
    this.uom3 = uom3;
    this.uom4 = uom4;
    this.convfact1 = convfact1;
    this.convfact2 = convfact2;
    this.convfact3 = convfact3;

    this.basePrice = basePrice;
    this.hpp = hpp;

    this.basePriceWithTax = basePriceWithTax;
    this.hppWithTax = hppWithTax;

    this.hpp_distributor = hpp_distributor;
    this.hpp_agen = hpp_agen;
    this.hpp_resseler_1 = hpp_resseler_1;
    this.hpp_resseler_2 = hpp_resseler_2;

    this.weightSmalest = weightSmalest;
    this.caseWeight = caseWeight;

    // this.materialType = materialType;

    this.fmaterialGroup3Bean = fmaterialGroup3Bean;
    this.fmaterialSalesBrandBean = fmaterialSalesBrandBean;
    this.ftaxBean = ftaxBean;
    this.taxable = taxable;
    this.fvendorBean = fvendorBean;
    this.fdivisionBean = fdivisionBean;

    this.weightSmalest = weightSmalest;
    this.caseWeight = caseWeight;

    this.distributionChannels = distributionChannels;
    this.fwarehouses = fwarehouses;
    this.materialMaps = materialMaps;
    this.prices = prices;
  }
}
