export default class FMaterialGroup2 {
  constructor(
    id,
    kode1,
    description,
    avatarImage,
    fmaterialGroup1Bean,
    statusActive=true,

    created,
    modified,
    modifiedBy
  ) {
    this.id = id;
    this.kode1 = kode1;
    this.description = description;
    this.avatarImage = avatarImage;
    this.statusActive = statusActive;
    this.fmaterialGroup1Bean = fmaterialGroup1Bean;
    this.created = created;
    this.modified = modified;
    this.modifiedBy = modifiedBy;
  }

}
