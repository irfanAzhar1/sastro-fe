export default class FMaterialGroup3 {
  constructor(
    id,
    kode1,
    description,
    avatarImage,
    fmaterialGroup2Bean,
    statusActive=true,

    created,
    modified,
    modifiedBy
  ) {
    this.id = id;
    this.kode1 = kode1;
    this.description = description;
    this.avatarImage = avatarImage;
    this.statusActive = statusActive;
    this.fmaterialGroup2Bean = fmaterialGroup2Bean;
    this.created = created;
    this.modified = modified;
    this.modifiedBy = modifiedBy;
  }

}
