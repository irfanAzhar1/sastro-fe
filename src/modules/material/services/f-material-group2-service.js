import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FMaterialGroup2Service {
  getApiUrl() {
    return API_URL;
  }
  getAllFMaterialGroup2() {
    return axios.get(API_URL + `getAllFMaterialGroup2`);
  }
  getAllFMaterialGroup2Containing(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFMaterialGroup2Containing?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFMaterialGroup2ById(id) {
    return axios.get(API_URL + `getFMaterialGroup2ById/${id}`);
  }
  updateFMaterialGroup2(item) {
    return axios.put(API_URL + `updateFMaterialGroup2/${item.id}`, item);
  }
  createFMaterialGroup2(item) {
    return axios.post(API_URL + `createFMaterialGroup2`, item);
  }
  deleteFMaterialGroup2(id) {
    return axios.delete(API_URL + `deleteFMaterialGroup2/${id}`);
  }
  deleteAllFMaterialGroup2(itemIds) {
    return axios.delete(API_URL + `deleteAllFMaterialGroup2`, {
      data: itemIds,
    });
  }
}
export default new FMaterialGroup2Service();
