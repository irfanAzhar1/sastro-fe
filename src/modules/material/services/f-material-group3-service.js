import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FMaterialGroup3Service {
  getApiUrl() {
    return API_URL;
  }
  getAllFMaterialGroup3() {
    return axios.get(API_URL + `getAllFMaterialGroup3`);
  }
  getAllFMaterialGroup3Containing(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFMaterialGroup3Containing?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFMaterialGroup3ById(id) {
    return axios.get(API_URL + `getFMaterialGroup3ById/${id}`);
  }
  updateFMaterialGroup3(item) {
    return axios.put(API_URL + `updateFMaterialGroup3/${item.id}`, item);
  }
  createFMaterialGroup3(item) {
    return axios.post(API_URL + `createFMaterialGroup3`, item);
  }
  deleteFMaterialGroup3(id) {
    return axios.delete(API_URL + `deleteFMaterialGroup3/${id}`);
  }
  deleteAllFMaterialGroup3(itemIds) {
    return axios.delete(API_URL + `deleteAllFMaterialGroup3`, {
      data: itemIds,
    });
  }
}
export default new FMaterialGroup3Service();
