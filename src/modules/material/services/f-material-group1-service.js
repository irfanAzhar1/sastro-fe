import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FMaterialGroup1Service {
  getApiUrl() {
    return API_URL;
  }
  getAllFMaterialGroup1() {
    return axios.get(API_URL + `getAllFMaterialGroup1`);
  }
  getAllFMaterialGroup1Containing(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFMaterialGroup1Containing?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFMaterialGroup1ById(id) {
    return axios.get(API_URL + `getFMaterialGroup1ById/${id}`);
  }
  updateFMaterialGroup1(item) {
    return axios.put(API_URL + `updateFMaterialGroup1/${item.id}`, item);
  }
  createFMaterialGroup1(item) {
    return axios.post(API_URL + `createFMaterialGroup1`, item);
  }
  deleteFMaterialGroup1(id) {
    return axios.delete(API_URL + `deleteFMaterialGroup1/${id}`);
  }
  deleteAllFMaterialGroup1(itemIds) {
    return axios.delete(API_URL + `deleteAllFMaterialGroup1`, {
      data: itemIds,
    });
  }
  getAllMaterialGroup1AllUser(){
    return axios.get(API_URL + `getAllFMaterialGroup1AllUser`)
  }
}
export default new FMaterialGroup1Service();
