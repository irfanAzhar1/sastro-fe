import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.VITE_API_SERVICE_URL_V1;

class FMaterialService {
  getApiUrl() {
    return API_URL;
  }
  getAllFMaterial() {
    return axios.get(this.getApiUrl() + `products`);
  }

  getAllFMaterialFilter() {
    return axios.get(this.getApiUrl() + `products/filter`);
  }
  getAllFMaterialByPnamePcode(query) {
    return axios.get(
      this.getApiUrl() + `products/productsByPnamePcode?query=${query}`
    );
  }
  getAllFMaterialContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      this.getApiUrl() +
        `products?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFMaterialById(id) {
    return axios.get(this.getApiUrl() + `products/${id}`);
  }
  getFMaterialByIdTable(id) {
    return axios.get(this.getApiUrl() + `products/productTable/${id}`);
  }
  getProductPriceWithQty(search, qty, warehouse) {
    return axios.get(
      this.getApiUrl() +
        `products/prices?query=${search}&qty=${qty}&warehouse=${warehouse}`
    );
  }
  updateFMaterial(id, item) {
    return axios.put(this.getApiUrl() + `products/${id}`, item);
  }
  createFMaterial(item) {
    return axios.post(this.getApiUrl() + `products`, item);
  }
  deleteFMaterial(id) {
    return axios.delete(this.getApiUrl() + `products/${id}`);
  }
  deleteAllFMaterial(itemIds) {
    return axios.delete(this.getApiUrl() + `deleteProductByIds`, {
      data: itemIds,
    });
  }
}
export default new FMaterialService();
