import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FMaterialSalesBrandService {
  getApiUrl() {
    return API_URL;
  }
  getAllFMaterialSalesBrand() {
    return axios.get(API_URL + `getAllFMaterialSalesBrand`);
  }
  getAllFMaterialSalesBrandContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFMaterialSalesBrandContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFMaterialSalesBrandById(id) {
    return axios.get(API_URL + `getFMaterialSalesBrandById/${id}`);
  }
  updateFMaterialSalesBrand(item) {
    return axios.put(API_URL + `updateFMaterialSalesBrand/${item.id}`, item);
  }
  createFMaterialSalesBrand(item) {
    return axios.post(API_URL + `createFMaterialSalesBrand`, item);
  }
  deleteFMaterialSalesBrand(id) {
    return axios.delete(API_URL + `deleteFMaterialSalesBrand/${id}`);
  }
  deleteAllFMaterialSalesBrand(itemIds) {
    return axios.delete(API_URL + `deleteAllFMaterialSalesBrand`, {
      data: itemIds,
    });
  }
  getAllFMaterialSalesBrandAllUser(){
   return axios.get(API_URL + `getAllFMaterialSalesBrandAllUser`)
  }
}
export default new FMaterialSalesBrandService();
