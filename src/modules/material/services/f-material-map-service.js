import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FMaterialMapService {
  getApiUrl() {
    return API_URL;
  }
  getAllFMaterialMap() {
    return axios.get(API_URL + `getAllFMaterialMap`);
  }
  getAllFMaterialMapContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFMaterialMapContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFMaterialMapById(id) {
    return axios.get(API_URL + `getFMaterialMapById/${id}`);
  }
  getAllFMaterialMapByParent(id) {
    return axios.get(API_URL + `getAllFMaterialMapByParent/${id}`);
  }
  getAllFMaterialMapByParentIds(ids) {
    return axios.get(API_URL + `getAllFMaterialMapByParentIds/${ids}`);
  }
  updateFMaterialMap(item) {
    return axios.put(API_URL + `updateFMaterialMap/${item.id}`, item);
  }
  createFMaterialMap(item) {
    return axios.post(API_URL + `createFMaterialMap`, item);
  }
  deleteFMaterialMap(id) {
    return axios.delete(API_URL + `deleteFMaterialMap/${id}`);
  }
}
export default new FMaterialMapService();
