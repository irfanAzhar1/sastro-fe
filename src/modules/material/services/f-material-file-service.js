import axios from "@/plugins/axios";
import ConstApiUrls from "@/services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FMaterialFileService {
  getApiUrl() {
    return API_URL;
  }
  getAllFMaterialFile() {
    return axios.get(API_URL + `getAllFMaterialFile`);
  }
  getAllFMaterialFileContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFMaterialFileContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFMaterialFileById(id) {
    return axios.get(API_URL + `getFMaterialFileById/${id}`);
  }
  getAllFMaterialFileByParent(id) {
    return axios.get(API_URL + `getAllFMaterialFileByParent/${id}`);
  }
  updateFMaterialFile(item) {
    return axios.put(API_URL + `updateFMaterialFile/${item.id}`, item);
  }
  createFMaterialFile(item) {
    return axios.post(API_URL + `createFMaterialFile`, item);
  }
  deleteFMaterialFile(id) {
    return axios.delete(API_URL + `deleteFMaterialFile/${id}`);
  }
  deleteFMaterialFileAvatarByParent(fkegiatanBean) {
    return axios.delete(
      API_URL + `deleteFMaterialFileAvatarByParent/${fkegiatanBean}`
    );
  }
  createFMaterialFileAvatar(item) {
    return axios.post(API_URL + `createFMaterialFileAvatar`, item);
  }
  deleteAllFMaterialFile(itemIds) {
    return axios.delete(API_URL + `deleteAllFMaterialFile`, {
      data: itemIds,
    });
  }
}
export default new FMaterialFileService();
