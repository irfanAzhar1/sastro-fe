import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FTaxService {
  getApiUrl() {
    return API_URL;
  }
  getAllFTax() {
    return axios.get(API_URL + `getAllFTax`);
  }
  getAllFTaxContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFTaxContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFTaxById(id) {
    return axios.get(API_URL + `getFTaxById/${id}`);
  }
  updateFTax(item) {
    return axios.put(API_URL + `updateFTax/${item.id}`, item);
  }
  createFTax(item) {
    return axios.post(API_URL + `createFTax`, item);
  }
  deleteFTax(id) {
    return axios.delete(API_URL + `deleteFTax/${id}`);
  }
  deleteAllFTax(itemIds) {
    return axios.delete(API_URL + `deleteAllFTax`, {
      data: itemIds,
    });
  }
}
export default new FTaxService();
