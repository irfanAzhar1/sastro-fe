export default class FNotificationGroup {
   constructor(
     id,
     question,
     direct = '1',
     showQuestion = true,
     phone,
     user_id,
     userList = [],
   ) {
     this.id = id;
     this.question = question;
     this.showQuestion = showQuestion;
     this.direct = direct;
     this.phone = phone;
     this.user_id = user_id;
     this.userList = userList;
   }

 }
