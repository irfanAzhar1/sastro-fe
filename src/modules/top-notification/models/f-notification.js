export default class FNotification {
  constructor(
    id,
    label_id,
    allTeam = true,
    description = "",
  ) {
    this.id = id;
    this.label_id = label_id;
    this.allTeam = allTeam;
    this.description = description;
  }
}
