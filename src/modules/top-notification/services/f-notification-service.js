import axios from "@/plugins/axios";
import ConstApiUrls from "@/services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FNotificationService {
  getApiUrl() {
    return API_URL;
  }
  // group notification
  getAllFNotificationGroup() {
    return axios.get(API_URL + `getAllFNotificationGroup`);
  }
  getAllFNotificationGroupSimple() {
    return axios.get(API_URL + `getAllFNotificationGroupSimple`);
  }
  getAllFNotificationGroupContainingQuestion(
    page,
    pageSize,
    sortBy,
    order,
    search
  ) {
    return axios.get(
      API_URL +
        `getAllFGroupNotificationContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFNotificationGroupById(id) {
    return axios.get(API_URL + `getFNotificationGroupById/${id}`);
  }
  readFNotification(item) {
    return axios.post(API_URL + `readNotification`, item);
  }
  deleteFNotificationGroupById(id) {
    return axios.delete(API_URL + `deleteFGroupNotification/${id}`);
  }
  updateFNotificationGroup(item) {
    return axios.put(API_URL + `updateFGroupNotification/${item.id}`, item);
  }
  createFNotificationGroup(item) {
    return axios.post(API_URL + `createFGroupNotification`, item);
  }
  // group template
  getAllFNotificationTemplateContainingTemplate(
    page,
    pageSize,
    sortBy,
    order,
    search
  ) {
    return axios.get(
      API_URL +
        `getAllFGroupNotificationTemplateContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getAllFNotificationTemplateByGroupId(id) {
    return axios.get(API_URL + `getFGroupNotificationTemplateByGroupId/${id}`);
  }
  deleteFNotificationTemplateById(id) {
    return axios.delete(API_URL + `deleteFGroupNotificationTemplate/${id}`);
  }
  updateFNotificationTemplate(item) {
    return axios.put(API_URL + `updateFNotificationTemplate/${item.id}`, item);
  }
  createFNotificationTemplate(item) {
    return axios.post(API_URL + `createFGroupNotificationTemplate`, item);
  }
  // notification label
  getAllFNotificationLabelWithNotification() {
    return axios.get(API_URL + `getAllFLabelNotification`);
  }
  getAllFNotificationLabelSimple() {
    return axios.get(API_URL + `getAllFLabelNotificationSimple`);
  }
  getAllFLabelNotificationContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFLabelNotificationContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  deleteFLabelNotification(id) {
    return axios.delete(API_URL + `deleteFLabelNotification/${id}`);
  }
  updateFLabelNotification(item) {
    return axios.put(API_URL + `updateFLabelNotification/${item.id}`, item);
  }
  createFLabelNotification(item) {
    return axios.post(API_URL + `createFLabelNotification`, item);
  }
  // notification
  getAllNotificationContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllNotificationContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  deleteFNotification(id) {
    return axios.delete(API_URL + `deleteFNotification/${id}`);
  }
  updateFNotification(item) {
    return axios.put(API_URL + `updateFNotification/${item.id}`, item);
  }
  createFNotification(item) {
    return axios.post(API_URL + `createFNotification`, item);
  }
}
export default new FNotificationService();
