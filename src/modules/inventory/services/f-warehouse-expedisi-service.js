import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FWarehouseExpedisiService {
  getApiUrl() {
    return API_URL;
  }
  getAllFWarehouseExpedisi() {
    return axios.get(API_URL + `getAllFWarehouseExpedisi`);
  }
  getAllFWarehouseExpedisiAvailable() {
    return axios.get(API_URL + `getAllFWarehouseExpedisiAvailable`);
  }
  getAllFWarehouseExpedisiContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFWarehouseExpedisiContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFWarehouseExpedisiById(id) {
    return axios.get(API_URL + `getFWarehouseExpedisiById/${id}`);
  }
  getAllByFwarehouseBean(fwarehouseBean) {
    return axios.get(API_URL + `getAllByFwarehouseBean/${fwarehouseBean}`);
  }
  getAllByFexpedisiBean(fexpedisiBean) {
    return axios.get(API_URL + `getAllByFexpedisiBean/${fexpedisiBean}`);
  }
  getAllByFwarehouseBeanAndFexpedisiBean(fwarehouseBean, fexpedisiBean) {
    return axios.get(
      API_URL +
        `getAllByFwarehouseBeanAndFexpedisiBean/${fwarehouseBean}/${fexpedisiBean}`
    );
  }
  updateFWarehouseExpedisi(item) {
    return axios.put(API_URL + `updateFWarehouseExpedisi/${item.id}`, item);
  }
  createFWarehouseExpedisi(item) {
    return axios.post(API_URL + `createFWarehouseExpedisi`, item);
  }
  deleteFWarehouseExpedisi(id) {
    return axios.delete(API_URL + `deleteFWarehouseExpedisi/${id}`);
  }
  deleteFWarehouseExpedisiAvatarByParent(fkegiatanBean) {
    return axios.delete(
      API_URL + `deleteFWarehouseExpedisiAvatarByParent/${fkegiatanBean}`
    );
  }
  createFWarehouseExpedisiAvatar(item) {
    return axios.post(API_URL + `createFWarehouseExpedisiAvatar`, item);
  }
  deleteAllFWarehouseExpedisi(itemIds) {
    return axios.delete(API_URL + `deleteAllFWarehouseExpedisi`, {
      data: itemIds,
    });
  }
}
export default new FWarehouseExpedisiService();
