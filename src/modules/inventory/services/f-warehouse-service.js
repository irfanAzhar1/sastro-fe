import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FWarehouseService {
  getApiUrl() {
    return API_URL;
  }
  getAllFWarehouse() {
    return axios.get(API_URL + `getAllFWarehouse`);
  }
  getAllFWarehouseByCompany() {
    return axios.get(API_URL + `getAllFWarehouseByCompany`);
  }
  getAllFWarehouseContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFWarehouseContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getFWarehouseById(id) {
    return axios.get(API_URL + `getFWarehouseById/${id}`);
  }
  updateFWarehouse(item) {
    return axios.put(API_URL + `updateFWarehouse/${item.id}`, item);
  }
  createFWarehouse(item) {
    return axios.post(API_URL + `createFWarehouse`, item);
  }
  deleteFWarehouse(id) {
    return axios.delete(API_URL + `deleteFWarehouse/${id}`);
  }
  deleteAllFWarehouse(itemIds) {
    return axios.delete(API_URL + `deleteAllFWarehouse`, {
      data: itemIds,
    });
  }
}
export default new FWarehouseService();
