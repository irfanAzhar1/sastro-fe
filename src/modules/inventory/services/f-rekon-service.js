import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.API_SERVICE_URL;

class FRekonService {
  getApiUrl() {
    return API_URL;
  }
  getRekonById(id) {
    return axios.get(API_URL + `findRekonById/${id}`);
  }
  getAllRekonContaining(page, pageSize, sortBy, order, search) {
    return axios.get(
      API_URL +
        `getAllFRekonContaining?page=${page}&pageSize=${pageSize}&sortBy=${sortBy}&order=${order}&search=${search}`
    );
  }
  getAllFRekonContainingExt(item) {
    return axios.post(API_URL + `getAllFRekonContainingExt`, item);
  }
  createRekonByAwbs(item) {
    return axios.post(API_URL + `createRekonByAwbs`, item);
  }
  updateFRekonByImport(item) {
    return axios.post(API_URL + `updateStatusRekonByAwbs`, item);
  }
}
export default new FRekonService();
