import axios from "@/plugins/axios";
import ConstApiUrls from "../../../services/const-api-urls";

const API_URL = ConstApiUrls.VITE_API_SERVICE_URL_V1;

class FReturnService {
  getApiUrl() {
    return API_URL;
  }
  getReturnById(id) {
    return axios.get(API_URL + `return/${id}`);
  }
  scan(items) {
    return axios.post(API_URL + `return/scan`, items);
  }
  export(items) {
    return axios.post(API_URL + `return/export`, items);
  }
  getReturn(items) {
    return axios.post(API_URL + `return/data`, items);
  }
  getSummary(items) {
    return axios.post(API_URL + `return/summary`, items);
  }
  create(items) {
    return axios.post(API_URL + `return`, items);
  }
  updateStatusByDeliveryNumber(items) {
    return axios.put(API_URL + `return/update_status`, items);
  }
}
export default new FReturnService();
